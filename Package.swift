// swift-tools-version:5.3
import PackageDescription

let package = Package(
    name: "EquisenseBluetooth",
    platforms: [
        .iOS(.v13),
.watchOS("9.1")
    ],
    products: [
        .library(
            name: "EquisenseBluetooth",
            targets: ["EquisenseBluetooth"]
        ),
    ],
    targets: [
        .binaryTarget(
            name: "EquisenseBluetooth",
            path: "./EquisenseBluetooth.xcframework"
        ),
    ]
)
