#import <Foundation/NSArray.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSError.h>
#import <Foundation/NSObject.h>
#import <Foundation/NSSet.h>
#import <Foundation/NSString.h>
#import <Foundation/NSValue.h>

@class EquisenseBluetoothUuidUuid, EquisenseBluetoothCentralManager, EquisenseBluetoothCentralManagerState, EquisenseBluetoothEquisenseDevice, EquisenseBluetoothKotlinEnumCompanion, EquisenseBluetoothKotlinEnum<E>, EquisenseBluetoothKotlinArray<T>, EquisenseBluetoothControlCharacteristic, EquisenseBluetoothEquisenseDeviceDelegate, EquisenseBluetoothDiscoveredService, EquisenseBluetoothEquisenseDeviceType, EquisenseBluetoothEquisenseDeviceStatus, EquisenseBluetoothKotlinByteArray, EquisenseBluetoothWriteType, EquisenseBluetoothPayloadAbstract, EquisenseBluetoothRevision, EquisenseBluetoothSerial, EquisenseBluetoothSyncReq, EquisenseBluetoothSensorHardwareRevision, EquisenseBluetoothKotlinx_datetimeInstant, EquisenseBluetoothCharacteristic, EquisenseBluetoothControlInputOpcode, EquisenseBluetoothControlOpcode, EquisenseBluetoothService, EquisenseBluetoothCharacteristicAbstract, EquisenseBluetoothBatteryCharacteristic, EquisenseBluetoothManufacturerData, EquisenseBluetoothBluetooth, EquisenseBluetoothBluetoothAvailability, EquisenseBluetoothBluetoothAvailabilityAvailable, EquisenseBluetoothReason, EquisenseBluetoothBluetoothAvailabilityUnavailable, EquisenseBluetoothBluetoothBaseUuid, EquisenseBluetoothKotlinThrowable, EquisenseBluetoothKotlinException, EquisenseBluetoothBluetoothException, EquisenseBluetoothIOException, EquisenseBluetoothNotConnectedException, NSData, NSUUID, EquisenseBluetoothDiscoveredCharacteristic, CBCharacteristic, EquisenseBluetoothDiscoveredDescriptor, CBDescriptor, CBService, EquisenseBluetoothFilter, EquisenseBluetoothFilterAddress, EquisenseBluetoothFilterName, EquisenseBluetoothFilterNamePrefix, EquisenseBluetoothFilterService, EquisenseBluetoothLazyCharacteristic, EquisenseBluetoothLazyDescriptor, EquisenseBluetoothLogging, EquisenseBluetoothState, EquisenseBluetoothStateConnected, EquisenseBluetoothStateConnecting, EquisenseBluetoothStateConnectingBluetooth, EquisenseBluetoothStateConnectingObserves, EquisenseBluetoothStateConnectingServices, EquisenseBluetoothStateDisconnectedStatus, EquisenseBluetoothStateDisconnected, EquisenseBluetoothStateDisconnectedStatusCancelled, EquisenseBluetoothStateDisconnectedStatusCentralDisconnected, EquisenseBluetoothStateDisconnectedStatusConnectionLimitReached, EquisenseBluetoothStateDisconnectedStatusEncryptionTimedOut, EquisenseBluetoothStateDisconnectedStatusFailed, EquisenseBluetoothStateDisconnectedStatusL2CapFailure, EquisenseBluetoothStateDisconnectedStatusLinkManagerProtocolTimeout, EquisenseBluetoothStateDisconnectedStatusPeripheralDisconnected, EquisenseBluetoothStateDisconnectedStatusTimeout, EquisenseBluetoothStateDisconnectedStatusUnknown, EquisenseBluetoothStateDisconnectedStatusUnknownDevice, EquisenseBluetoothStateDisconnecting, EquisenseBluetoothLoggingFormat, EquisenseBluetoothLoggingLevel, EquisenseBluetoothSystemLogEngine, EquisenseBluetoothKotlinByteIterator, EquisenseBluetoothScannerBuilder, EquisenseBluetoothPeripheralBuilder, CBPeripheral, EquisenseBluetoothHexBuilder, EquisenseBluetoothKotlinRuntimeException, EquisenseBluetoothKotlinIllegalStateException, EquisenseBluetoothKotlinx_datetimeInstantCompanion, EquisenseBluetoothKotlinx_serialization_coreSerializersModule, EquisenseBluetoothKotlinx_serialization_coreSerialKind, EquisenseBluetoothKotlinNothing;

@protocol EquisenseBluetoothAdvertisementWrapperAbstract, EquisenseBluetoothAdvertisement, EquisenseBluetoothCentralManagerDelegate, EquisenseBluetoothScanner, EquisenseBluetoothKotlinComparable, EquisenseBluetoothPeripheralWrapperAbstract, EquisenseBluetoothCharacteristic, EquisenseBluetoothKotlinSuspendFunction0, EquisenseBluetoothDescriptor, EquisenseBluetoothPeripheral, EquisenseBluetoothKotlinx_coroutines_coreCoroutineScope, EquisenseBluetoothKotlinx_coroutines_coreMutableStateFlow, EquisenseBluetoothKotlinx_coroutines_coreFlow, EquisenseBluetoothKotlinx_coroutines_coreStateFlow, EquisenseBluetoothService, EquisenseBluetoothKotlinSuspendFunction2, EquisenseBluetoothKotlinSuspendFunction1, EquisenseBluetoothLoggingDataProcessor, EquisenseBluetoothLogEngine, EquisenseBluetoothCoreBluetoothScanner, EquisenseBluetoothCoreBluetoothPeripheral, EquisenseBluetoothKotlinIterator, EquisenseBluetoothKotlinFunction, EquisenseBluetoothKotlinCoroutineContext, EquisenseBluetoothKotlinx_coroutines_coreFlowCollector, EquisenseBluetoothKotlinx_coroutines_coreSharedFlow, EquisenseBluetoothKotlinx_coroutines_coreMutableSharedFlow, EquisenseBluetoothKotlinx_serialization_coreKSerializer, EquisenseBluetoothKotlinCoroutineContextElement, EquisenseBluetoothKotlinCoroutineContextKey, EquisenseBluetoothKotlinx_serialization_coreEncoder, EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor, EquisenseBluetoothKotlinx_serialization_coreSerializationStrategy, EquisenseBluetoothKotlinx_serialization_coreDecoder, EquisenseBluetoothKotlinx_serialization_coreDeserializationStrategy, EquisenseBluetoothKotlinx_serialization_coreCompositeEncoder, EquisenseBluetoothKotlinAnnotation, EquisenseBluetoothKotlinx_serialization_coreCompositeDecoder, EquisenseBluetoothKotlinx_serialization_coreSerializersModuleCollector, EquisenseBluetoothKotlinKClass, EquisenseBluetoothKotlinKDeclarationContainer, EquisenseBluetoothKotlinKAnnotatedElement, EquisenseBluetoothKotlinKClassifier;

NS_ASSUME_NONNULL_BEGIN
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-warning-option"
#pragma clang diagnostic ignored "-Wincompatible-property-type"
#pragma clang diagnostic ignored "-Wnullability"

#pragma push_macro("_Nullable_result")
#if !__has_feature(nullability_nullable_result)
#undef _Nullable_result
#define _Nullable_result _Nullable
#endif

__attribute__((swift_name("KotlinBase")))
@interface EquisenseBluetoothBase : NSObject
- (instancetype)init __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
+ (void)initialize __attribute__((objc_requires_super));
@end

@interface EquisenseBluetoothBase (EquisenseBluetoothBaseCopying) <NSCopying>
@end

__attribute__((swift_name("KotlinMutableSet")))
@interface EquisenseBluetoothMutableSet<ObjectType> : NSMutableSet<ObjectType>
@end

__attribute__((swift_name("KotlinMutableDictionary")))
@interface EquisenseBluetoothMutableDictionary<KeyType, ObjectType> : NSMutableDictionary<KeyType, ObjectType>
@end

@interface NSError (NSErrorEquisenseBluetoothKotlinException)
@property (readonly) id _Nullable kotlinException;
@end

__attribute__((swift_name("KotlinNumber")))
@interface EquisenseBluetoothNumber : NSNumber
- (instancetype)initWithChar:(char)value __attribute__((unavailable));
- (instancetype)initWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
- (instancetype)initWithShort:(short)value __attribute__((unavailable));
- (instancetype)initWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
- (instancetype)initWithInt:(int)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
- (instancetype)initWithLong:(long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
- (instancetype)initWithLongLong:(long long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
- (instancetype)initWithFloat:(float)value __attribute__((unavailable));
- (instancetype)initWithDouble:(double)value __attribute__((unavailable));
- (instancetype)initWithBool:(BOOL)value __attribute__((unavailable));
- (instancetype)initWithInteger:(NSInteger)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
+ (instancetype)numberWithChar:(char)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
+ (instancetype)numberWithShort:(short)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
+ (instancetype)numberWithInt:(int)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
+ (instancetype)numberWithLong:(long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
+ (instancetype)numberWithLongLong:(long long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
+ (instancetype)numberWithFloat:(float)value __attribute__((unavailable));
+ (instancetype)numberWithDouble:(double)value __attribute__((unavailable));
+ (instancetype)numberWithBool:(BOOL)value __attribute__((unavailable));
+ (instancetype)numberWithInteger:(NSInteger)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
@end

__attribute__((swift_name("KotlinByte")))
@interface EquisenseBluetoothByte : EquisenseBluetoothNumber
- (instancetype)initWithChar:(char)value;
+ (instancetype)numberWithChar:(char)value;
@end

__attribute__((swift_name("KotlinUByte")))
@interface EquisenseBluetoothUByte : EquisenseBluetoothNumber
- (instancetype)initWithUnsignedChar:(unsigned char)value;
+ (instancetype)numberWithUnsignedChar:(unsigned char)value;
@end

__attribute__((swift_name("KotlinShort")))
@interface EquisenseBluetoothShort : EquisenseBluetoothNumber
- (instancetype)initWithShort:(short)value;
+ (instancetype)numberWithShort:(short)value;
@end

__attribute__((swift_name("KotlinUShort")))
@interface EquisenseBluetoothUShort : EquisenseBluetoothNumber
- (instancetype)initWithUnsignedShort:(unsigned short)value;
+ (instancetype)numberWithUnsignedShort:(unsigned short)value;
@end

__attribute__((swift_name("KotlinInt")))
@interface EquisenseBluetoothInt : EquisenseBluetoothNumber
- (instancetype)initWithInt:(int)value;
+ (instancetype)numberWithInt:(int)value;
@end

__attribute__((swift_name("KotlinUInt")))
@interface EquisenseBluetoothUInt : EquisenseBluetoothNumber
- (instancetype)initWithUnsignedInt:(unsigned int)value;
+ (instancetype)numberWithUnsignedInt:(unsigned int)value;
@end

__attribute__((swift_name("KotlinLong")))
@interface EquisenseBluetoothLong : EquisenseBluetoothNumber
- (instancetype)initWithLongLong:(long long)value;
+ (instancetype)numberWithLongLong:(long long)value;
@end

__attribute__((swift_name("KotlinULong")))
@interface EquisenseBluetoothULong : EquisenseBluetoothNumber
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value;
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value;
@end

__attribute__((swift_name("KotlinFloat")))
@interface EquisenseBluetoothFloat : EquisenseBluetoothNumber
- (instancetype)initWithFloat:(float)value;
+ (instancetype)numberWithFloat:(float)value;
@end

__attribute__((swift_name("KotlinDouble")))
@interface EquisenseBluetoothDouble : EquisenseBluetoothNumber
- (instancetype)initWithDouble:(double)value;
+ (instancetype)numberWithDouble:(double)value;
@end

__attribute__((swift_name("KotlinBoolean")))
@interface EquisenseBluetoothBoolean : EquisenseBluetoothNumber
- (instancetype)initWithBool:(BOOL)value;
+ (instancetype)numberWithBool:(BOOL)value;
@end

__attribute__((swift_name("AdvertisementWrapperAbstract")))
@protocol EquisenseBluetoothAdvertisementWrapperAbstract
@required
@property (readonly) int32_t rssi __attribute__((swift_name("rssi")));
@property (readonly) NSArray<EquisenseBluetoothUuidUuid *> *uuids __attribute__((swift_name("uuids")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AdvertisementWrapper")))
@interface EquisenseBluetoothAdvertisementWrapper : EquisenseBluetoothBase <EquisenseBluetoothAdvertisementWrapperAbstract>
- (instancetype)initWithAdvertisement:(id<EquisenseBluetoothAdvertisement>)advertisement __attribute__((swift_name("init(advertisement:)"))) __attribute__((objc_designated_initializer));
@property (readonly) int32_t rssi __attribute__((swift_name("rssi")));
@property (readonly) NSArray<EquisenseBluetoothUuidUuid *> *uuids __attribute__((swift_name("uuids")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BluetoothManager")))
@interface EquisenseBluetoothBluetoothManager : EquisenseBluetoothBase
- (instancetype)initWithDelegate:(id<EquisenseBluetoothCentralManagerDelegate>)delegate __attribute__((swift_name("init(delegate:)"))) __attribute__((objc_designated_initializer));
- (void)start __attribute__((swift_name("start()")));
- (void)stop __attribute__((swift_name("stop()")));
@property (readonly) EquisenseBluetoothCentralManager *centralManager __attribute__((swift_name("centralManager")));
@property BOOL isScanning __attribute__((swift_name("isScanning")));
@property (readonly) id<EquisenseBluetoothScanner> scanner __attribute__((swift_name("scanner")));
@end

__attribute__((unavailable("Kotlin subclass of Objective-C class can't be imported")))
__attribute__((swift_name("CBCentralManagerImplementation")))
@interface EquisenseBluetoothCBCentralManagerImplementation : NSObject
@end

__attribute__((swift_name("CentralManager")))
@interface EquisenseBluetoothCentralManager : EquisenseBluetoothBase
- (instancetype)initWithDelegate:(id<EquisenseBluetoothCentralManagerDelegate>)delegate __attribute__((swift_name("init(delegate:)"))) __attribute__((objc_designated_initializer));
@property (readonly) id<EquisenseBluetoothCentralManagerDelegate> delegate __attribute__((swift_name("delegate")));
@property BOOL isScanning __attribute__((swift_name("isScanning")));
@end

__attribute__((swift_name("CentralManagerDelegate")))
@protocol EquisenseBluetoothCentralManagerDelegate
@required
- (void)bluetoothStatusDidUpdateStatus:(EquisenseBluetoothCentralManagerState *)status __attribute__((swift_name("bluetoothStatusDidUpdate(status:)")));
- (void)didDeviceFoundDevice:(EquisenseBluetoothEquisenseDevice *)device __attribute__((swift_name("didDeviceFound(device:)")));
@end

__attribute__((swift_name("KotlinComparable")))
@protocol EquisenseBluetoothKotlinComparable
@required
- (int32_t)compareToOther:(id _Nullable)other __attribute__((swift_name("compareTo(other:)")));
@end

__attribute__((swift_name("KotlinEnum")))
@interface EquisenseBluetoothKotlinEnum<E> : EquisenseBluetoothBase <EquisenseBluetoothKotlinComparable>
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) EquisenseBluetoothKotlinEnumCompanion *companion __attribute__((swift_name("companion")));
- (int32_t)compareToOther:(E)other __attribute__((swift_name("compareTo(other:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) int32_t ordinal __attribute__((swift_name("ordinal")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CentralManagerState")))
@interface EquisenseBluetoothCentralManagerState : EquisenseBluetoothKotlinEnum<EquisenseBluetoothCentralManagerState *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) EquisenseBluetoothCentralManagerState *poweredon __attribute__((swift_name("poweredon")));
@property (class, readonly) EquisenseBluetoothCentralManagerState *poweredoff __attribute__((swift_name("poweredoff")));
@property (class, readonly) EquisenseBluetoothCentralManagerState *resetting __attribute__((swift_name("resetting")));
@property (class, readonly) EquisenseBluetoothCentralManagerState *unauthorized __attribute__((swift_name("unauthorized")));
@property (class, readonly) EquisenseBluetoothCentralManagerState *unknown __attribute__((swift_name("unknown")));
@property (class, readonly) EquisenseBluetoothCentralManagerState *unsupported __attribute__((swift_name("unsupported")));
+ (EquisenseBluetoothKotlinArray<EquisenseBluetoothCentralManagerState *> *)values __attribute__((swift_name("values()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EquisenseDevice")))
@interface EquisenseBluetoothEquisenseDevice : EquisenseBluetoothBase
- (instancetype)initWithPeripheral:(id<EquisenseBluetoothPeripheralWrapperAbstract>)peripheral advertisement:(id<EquisenseBluetoothAdvertisementWrapperAbstract>)advertisement __attribute__((swift_name("init(peripheral:advertisement:)"))) __attribute__((objc_designated_initializer));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)connectWithCompletionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("connect(completionHandler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)disconnectWithCompletionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("disconnect(completionHandler:)")));
@property void (^ _Nullable batteryLevel)(EquisenseBluetoothInt *) __attribute__((swift_name("batteryLevel")));
@property (readonly) EquisenseBluetoothControlCharacteristic *control __attribute__((swift_name("control")));
@property EquisenseBluetoothEquisenseDeviceDelegate * _Nullable delegate __attribute__((swift_name("delegate")));
@property (readonly) NSString * _Nullable name __attribute__((swift_name("name")));
@property (readonly) int32_t rssi __attribute__((swift_name("rssi")));
@property (readonly) NSArray<EquisenseBluetoothDiscoveredService *> *services __attribute__((swift_name("services")));
@property (readonly) EquisenseBluetoothEquisenseDeviceType *type __attribute__((swift_name("type")));
@end

__attribute__((swift_name("EquisenseDeviceDelegate")))
@interface EquisenseBluetoothEquisenseDeviceDelegate : EquisenseBluetoothBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)statusUpdatedStatus:(EquisenseBluetoothEquisenseDeviceStatus *)status __attribute__((swift_name("statusUpdated(status:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EquisenseDeviceStatus")))
@interface EquisenseBluetoothEquisenseDeviceStatus : EquisenseBluetoothKotlinEnum<EquisenseBluetoothEquisenseDeviceStatus *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) EquisenseBluetoothEquisenseDeviceStatus *idle __attribute__((swift_name("idle")));
@property (class, readonly) EquisenseBluetoothEquisenseDeviceStatus *connected __attribute__((swift_name("connected")));
@property (class, readonly) EquisenseBluetoothEquisenseDeviceStatus *disconnected __attribute__((swift_name("disconnected")));
+ (EquisenseBluetoothKotlinArray<EquisenseBluetoothEquisenseDeviceStatus *> *)values __attribute__((swift_name("values()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("EquisenseDeviceType")))
@interface EquisenseBluetoothEquisenseDeviceType : EquisenseBluetoothKotlinEnum<EquisenseBluetoothEquisenseDeviceType *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) EquisenseBluetoothEquisenseDeviceType *unknown __attribute__((swift_name("unknown")));
@property (class, readonly) EquisenseBluetoothEquisenseDeviceType *motion __attribute__((swift_name("motion")));
@property (class, readonly) EquisenseBluetoothEquisenseDeviceType *bootloader __attribute__((swift_name("bootloader")));
+ (EquisenseBluetoothKotlinArray<EquisenseBluetoothEquisenseDeviceType *> *)values __attribute__((swift_name("values()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IOSCentralManager")))
@interface EquisenseBluetoothIOSCentralManager : EquisenseBluetoothCentralManager
- (instancetype)initWithDelegate:(id<EquisenseBluetoothCentralManagerDelegate>)delegate __attribute__((swift_name("init(delegate:)"))) __attribute__((objc_designated_initializer));
@property BOOL isScanning __attribute__((swift_name("isScanning")));
@end

__attribute__((swift_name("PeripheralWrapperAbstract")))
@protocol EquisenseBluetoothPeripheralWrapperAbstract
@required

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)connectWithCompletionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("connect(completionHandler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)disconnectWithCompletionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("disconnect(completionHandler:)")));
- (void)observeCharacteristic:(id<EquisenseBluetoothCharacteristic>)characteristic onSubscription:(id<EquisenseBluetoothKotlinSuspendFunction0> _Nullable)onSubscription onRead:(void (^)(EquisenseBluetoothKotlinByteArray *))onRead __attribute__((swift_name("observe(characteristic:onSubscription:onRead:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)readCharacteristic:(id<EquisenseBluetoothCharacteristic>)characteristic completionHandler:(void (^)(EquisenseBluetoothKotlinByteArray * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("read(characteristic:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)writeCharacteristic:(id<EquisenseBluetoothCharacteristic>)characteristic data:(EquisenseBluetoothKotlinByteArray *)data writeType:(EquisenseBluetoothWriteType *)writeType completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("write(characteristic:data:writeType:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)writeDescriptor:(id<EquisenseBluetoothDescriptor>)descriptor data:(EquisenseBluetoothKotlinByteArray *)data completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("write(descriptor:data:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)writeAndReadCharacteristic:(id<EquisenseBluetoothCharacteristic>)characteristic data:(EquisenseBluetoothKotlinByteArray *)data completionHandler:(void (^)(EquisenseBluetoothKotlinByteArray * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("writeAndRead(characteristic:data:completionHandler:)")));
@property (readonly) NSString * _Nullable name __attribute__((swift_name("name")));
@property (readonly) NSArray<EquisenseBluetoothDiscoveredService *> * _Nullable services __attribute__((swift_name("services")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PeripheralWrapper")))
@interface EquisenseBluetoothPeripheralWrapper : EquisenseBluetoothBase <EquisenseBluetoothPeripheralWrapperAbstract>
- (instancetype)initWithPeripheral:(id<EquisenseBluetoothPeripheral>)peripheral __attribute__((swift_name("init(peripheral:)"))) __attribute__((objc_designated_initializer));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)connectWithCompletionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("connect(completionHandler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)disconnectWithCompletionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("disconnect(completionHandler:)")));
- (void)observeCharacteristic:(id<EquisenseBluetoothCharacteristic>)characteristic onSubscription:(id<EquisenseBluetoothKotlinSuspendFunction0> _Nullable)onSubscription onRead:(void (^)(EquisenseBluetoothKotlinByteArray *))onRead __attribute__((swift_name("observe(characteristic:onSubscription:onRead:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)readCharacteristic:(id<EquisenseBluetoothCharacteristic>)characteristic completionHandler:(void (^)(EquisenseBluetoothKotlinByteArray * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("read(characteristic:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)writeCharacteristic:(id<EquisenseBluetoothCharacteristic>)characteristic data:(EquisenseBluetoothKotlinByteArray *)data writeType:(EquisenseBluetoothWriteType *)writeType completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("write(characteristic:data:writeType:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)writeDescriptor:(id<EquisenseBluetoothDescriptor>)descriptor data:(EquisenseBluetoothKotlinByteArray *)data completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("write(descriptor:data:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)writeAndReadCharacteristic:(id<EquisenseBluetoothCharacteristic>)characteristic data:(EquisenseBluetoothKotlinByteArray *)data completionHandler:(void (^)(EquisenseBluetoothKotlinByteArray * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("writeAndRead(characteristic:data:completionHandler:)")));
@property (readonly) NSString * _Nullable name __attribute__((swift_name("name")));
@property (readonly) NSArray<EquisenseBluetoothDiscoveredService *> * _Nullable services __attribute__((swift_name("services")));
@end

__attribute__((swift_name("PayloadAbstract")))
@interface EquisenseBluetoothPayloadAbstract : EquisenseBluetoothBase
- (instancetype)initWithByteArray:(EquisenseBluetoothKotlinByteArray *)byteArray __attribute__((swift_name("init(byteArray:)"))) __attribute__((objc_designated_initializer));
@property (readonly) int8_t length __attribute__((swift_name("length")));
@property (readonly) int8_t opcode __attribute__((swift_name("opcode")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Revision")))
@interface EquisenseBluetoothRevision : EquisenseBluetoothPayloadAbstract
- (instancetype)initWithByteArray:(EquisenseBluetoothKotlinByteArray *)byteArray __attribute__((swift_name("init(byteArray:)"))) __attribute__((objc_designated_initializer));
- (EquisenseBluetoothRevision *)doCopyByteArray:(EquisenseBluetoothKotlinByteArray *)byteArray __attribute__((swift_name("doCopy(byteArray:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) uint32_t bootloaderVersion __attribute__((swift_name("bootloaderVersion")));
@property (readonly) EquisenseBluetoothKotlinByteArray *byteArray __attribute__((swift_name("byteArray")));
@property (readonly) uint32_t hardwareVersion __attribute__((swift_name("hardwareVersion")));
@property (readonly) BOOL isFirmwareDebug __attribute__((swift_name("isFirmwareDebug")));
@property (readonly) uint32_t major __attribute__((swift_name("major")));
@property (readonly) uint32_t minor __attribute__((swift_name("minor")));
@property (readonly) uint32_t patch __attribute__((swift_name("patch")));
@property (readonly) NSString *qualifier __attribute__((swift_name("qualifier")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Serial")))
@interface EquisenseBluetoothSerial : EquisenseBluetoothBase
- (instancetype)initWithByteArray:(EquisenseBluetoothKotlinByteArray *)byteArray __attribute__((swift_name("init(byteArray:)"))) __attribute__((objc_designated_initializer));
- (EquisenseBluetoothSerial *)doCopyByteArray:(EquisenseBluetoothKotlinByteArray *)byteArray __attribute__((swift_name("doCopy(byteArray:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) uint64_t value __attribute__((swift_name("value")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SyncMemStatus")))
@interface EquisenseBluetoothSyncMemStatus : EquisenseBluetoothPayloadAbstract
- (instancetype)initWithByteArray:(EquisenseBluetoothKotlinByteArray *)byteArray __attribute__((swift_name("init(byteArray:)"))) __attribute__((objc_designated_initializer));
@property (readonly) EquisenseBluetoothKotlinByteArray *byteArray __attribute__((swift_name("byteArray")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SyncReq")))
@interface EquisenseBluetoothSyncReq : EquisenseBluetoothPayloadAbstract
- (instancetype)initWithByteArray:(EquisenseBluetoothKotlinByteArray *)byteArray __attribute__((swift_name("init(byteArray:)"))) __attribute__((objc_designated_initializer));
- (EquisenseBluetoothSyncReq *)doCopyByteArray:(EquisenseBluetoothKotlinByteArray *)byteArray __attribute__((swift_name("doCopy(byteArray:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) EquisenseBluetoothKotlinByteArray *byteArray __attribute__((swift_name("byteArray")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SysUptime")))
@interface EquisenseBluetoothSysUptime : EquisenseBluetoothPayloadAbstract
- (instancetype)initWithByteArray:(EquisenseBluetoothKotlinByteArray *)byteArray __attribute__((swift_name("init(byteArray:)"))) __attribute__((objc_designated_initializer));
@property (readonly) EquisenseBluetoothKotlinByteArray *byteArray __attribute__((swift_name("byteArray")));
@property (readonly) uint32_t lastUptime __attribute__((swift_name("lastUptime")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SensorHardwareRevision")))
@interface EquisenseBluetoothSensorHardwareRevision : EquisenseBluetoothKotlinEnum<EquisenseBluetoothSensorHardwareRevision *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) EquisenseBluetoothSensorHardwareRevision *motion __attribute__((swift_name("motion")));
@property (class, readonly) EquisenseBluetoothSensorHardwareRevision *motions __attribute__((swift_name("motions")));
@property (class, readonly) EquisenseBluetoothSensorHardwareRevision *saddlewithmotion __attribute__((swift_name("saddlewithmotion")));
@property (class, readonly) EquisenseBluetoothSensorHardwareRevision *saddlewithmotions __attribute__((swift_name("saddlewithmotions")));
+ (EquisenseBluetoothKotlinArray<EquisenseBluetoothSensorHardwareRevision *> *)values __attribute__((swift_name("values()")));
@property (readonly) int32_t value __attribute__((swift_name("value")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Session")))
@interface EquisenseBluetoothSession : EquisenseBluetoothBase
- (instancetype)initWithId:(NSString *)id motionID:(int32_t)motionID messageID:(NSString * _Nullable)messageID syncDone:(EquisenseBluetoothBoolean * _Nullable)syncDone duration:(int64_t)duration stoppedAt:(EquisenseBluetoothKotlinx_datetimeInstant * _Nullable)stoppedAt horseWeight:(EquisenseBluetoothInt * _Nullable)horseWeight sensorSerial:(NSString * _Nullable)sensorSerial firmwareVersion:(NSString *)firmwareVersion hardwareRevision:(EquisenseBluetoothSensorHardwareRevision *)hardwareRevision appVersion:(NSString *)appVersion opened:(BOOL)opened __attribute__((swift_name("init(id:motionID:messageID:syncDone:duration:stoppedAt:horseWeight:sensorSerial:firmwareVersion:hardwareRevision:appVersion:opened:)"))) __attribute__((objc_designated_initializer));
@property (readonly) NSString *appVersion __attribute__((swift_name("appVersion")));
@property (readonly) int64_t duration __attribute__((swift_name("duration")));
@property (readonly) NSString *firmwareVersion __attribute__((swift_name("firmwareVersion")));
@property (readonly) EquisenseBluetoothSensorHardwareRevision *hardwareRevision __attribute__((swift_name("hardwareRevision")));
@property (readonly) EquisenseBluetoothInt * _Nullable horseWeight __attribute__((swift_name("horseWeight")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) NSString * _Nullable messageID __attribute__((swift_name("messageID")));
@property (readonly) int32_t motionID __attribute__((swift_name("motionID")));
@property (readonly) BOOL opened __attribute__((swift_name("opened")));
@property (readonly) NSString * _Nullable sensorSerial __attribute__((swift_name("sensorSerial")));
@property (readonly) EquisenseBluetoothKotlinx_datetimeInstant * _Nullable stoppedAt __attribute__((swift_name("stoppedAt")));
@property (readonly) EquisenseBluetoothBoolean * _Nullable syncDone __attribute__((swift_name("syncDone")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AdvertisementData")))
@interface EquisenseBluetoothAdvertisementData : EquisenseBluetoothBase
- (instancetype)initWithAdvertisement:(id<EquisenseBluetoothAdvertisement>)advertisement __attribute__((swift_name("init(advertisement:)"))) __attribute__((objc_designated_initializer));
@property (readonly) id<EquisenseBluetoothAdvertisement> advertisement __attribute__((swift_name("advertisement")));
@property (readonly) NSString * _Nullable name __attribute__((swift_name("name")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Characteristic_")))
@interface EquisenseBluetoothCharacteristic : EquisenseBluetoothKotlinEnum<EquisenseBluetoothCharacteristic *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) EquisenseBluetoothCharacteristic *battery __attribute__((swift_name("battery")));
@property (class, readonly) EquisenseBluetoothCharacteristic *control __attribute__((swift_name("control")));
@property (class, readonly) EquisenseBluetoothCharacteristic *data __attribute__((swift_name("data")));
@property (class, readonly) EquisenseBluetoothCharacteristic *rawimudata __attribute__((swift_name("rawimudata")));
@property (class, readonly) EquisenseBluetoothCharacteristic *rawecgdata __attribute__((swift_name("rawecgdata")));
+ (EquisenseBluetoothKotlinArray<EquisenseBluetoothCharacteristic *> *)values __attribute__((swift_name("values()")));
@property (readonly) NSString *uuid __attribute__((swift_name("uuid")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ControlInputOpcode")))
@interface EquisenseBluetoothControlInputOpcode : EquisenseBluetoothKotlinEnum<EquisenseBluetoothControlInputOpcode *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) EquisenseBluetoothControlInputOpcode *sysuptime __attribute__((swift_name("sysuptime")));
@property (class, readonly) EquisenseBluetoothControlInputOpcode *revision __attribute__((swift_name("revision")));
@property (class, readonly) EquisenseBluetoothControlInputOpcode *serial __attribute__((swift_name("serial")));
@property (class, readonly) EquisenseBluetoothControlInputOpcode *syncmemstatus __attribute__((swift_name("syncmemstatus")));
@property (class, readonly) EquisenseBluetoothControlInputOpcode *syncreq __attribute__((swift_name("syncreq")));
+ (EquisenseBluetoothKotlinArray<EquisenseBluetoothControlInputOpcode *> *)values __attribute__((swift_name("values()")));
@property (readonly) uint8_t opcode __attribute__((swift_name("opcode")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ControlOpcode")))
@interface EquisenseBluetoothControlOpcode : EquisenseBluetoothKotlinEnum<EquisenseBluetoothControlOpcode *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) EquisenseBluetoothControlOpcode *systime __attribute__((swift_name("systime")));
@property (class, readonly) EquisenseBluetoothControlOpcode *sysuptime __attribute__((swift_name("sysuptime")));
@property (class, readonly) EquisenseBluetoothControlOpcode *revision __attribute__((swift_name("revision")));
@property (class, readonly) EquisenseBluetoothControlOpcode *serial __attribute__((swift_name("serial")));
@property (class, readonly) EquisenseBluetoothControlOpcode *syncmemstatus __attribute__((swift_name("syncmemstatus")));
@property (class, readonly) EquisenseBluetoothControlOpcode *syncreq __attribute__((swift_name("syncreq")));
+ (EquisenseBluetoothKotlinArray<EquisenseBluetoothControlOpcode *> *)values __attribute__((swift_name("values()")));
@property (readonly) EquisenseBluetoothControlInputOpcode * _Nullable input __attribute__((swift_name("input")));
@property (readonly) EquisenseBluetoothKotlinByteArray *output __attribute__((swift_name("output")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Service")))
@interface EquisenseBluetoothService : EquisenseBluetoothKotlinEnum<EquisenseBluetoothService *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) EquisenseBluetoothService *battery __attribute__((swift_name("battery")));
@property (class, readonly) EquisenseBluetoothService *equisense __attribute__((swift_name("equisense")));
@property (class, readonly) EquisenseBluetoothService *bootloader __attribute__((swift_name("bootloader")));
@property (class, readonly) EquisenseBluetoothService *bootloaderlegacy __attribute__((swift_name("bootloaderlegacy")));
+ (EquisenseBluetoothKotlinArray<EquisenseBluetoothService *> *)values __attribute__((swift_name("values()")));
@property (readonly) NSString *uuid __attribute__((swift_name("uuid")));
@end

__attribute__((swift_name("CharacteristicAbstract")))
@interface EquisenseBluetoothCharacteristicAbstract : EquisenseBluetoothBase
- (instancetype)initWithCharacteristic:(id<EquisenseBluetoothCharacteristic>)characteristic peripheral:(id<EquisenseBluetoothPeripheralWrapperAbstract>)peripheral __attribute__((swift_name("init(characteristic:peripheral:)"))) __attribute__((objc_designated_initializer));
- (void)dispose __attribute__((swift_name("dispose()")));
- (void)onDeviceConnected __attribute__((swift_name("onDeviceConnected()")));
- (void)onReadCharacteristicOpcode:(uint8_t)opcode length:(int32_t)length byteArray:(EquisenseBluetoothKotlinByteArray *)byteArray __attribute__((swift_name("onReadCharacteristic(opcode:length:byteArray:)")));
- (void)startObserving __attribute__((swift_name("startObserving()")));

/**
 * @note This property has protected visibility in Kotlin source and is intended only for use by subclasses.
*/
@property (readonly) id<EquisenseBluetoothCharacteristic> characteristic __attribute__((swift_name("characteristic")));

/**
 * @note This property has protected visibility in Kotlin source and is intended only for use by subclasses.
*/
@property (readonly) id<EquisenseBluetoothKotlinx_coroutines_coreCoroutineScope> coroutineScope __attribute__((swift_name("coroutineScope")));

/**
 * @note This property has protected visibility in Kotlin source and is intended only for use by subclasses.
*/
@property id<EquisenseBluetoothKotlinSuspendFunction0> _Nullable onSubscription __attribute__((swift_name("onSubscription")));

/**
 * @note This property has protected visibility in Kotlin source and is intended only for use by subclasses.
*/
@property (readonly) id<EquisenseBluetoothPeripheralWrapperAbstract> peripheral __attribute__((swift_name("peripheral")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BatteryCharacteristic")))
@interface EquisenseBluetoothBatteryCharacteristic : EquisenseBluetoothCharacteristicAbstract
- (instancetype)initWithPeripheral:(id<EquisenseBluetoothPeripheralWrapperAbstract>)peripheral __attribute__((swift_name("init(peripheral:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCharacteristic:(id<EquisenseBluetoothCharacteristic>)characteristic peripheral:(id<EquisenseBluetoothPeripheralWrapperAbstract>)peripheral __attribute__((swift_name("init(characteristic:peripheral:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (EquisenseBluetoothBatteryCharacteristic *)doCopyPeripheral:(id<EquisenseBluetoothPeripheralWrapperAbstract>)peripheral __attribute__((swift_name("doCopy(peripheral:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (void)onDeviceConnected __attribute__((swift_name("onDeviceConnected()")));
- (void)onReadCharacteristicOpcode:(uint8_t)opcode length:(int32_t)length byteArray:(EquisenseBluetoothKotlinByteArray *)byteArray __attribute__((swift_name("onReadCharacteristic(opcode:length:byteArray:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id<EquisenseBluetoothKotlinx_coroutines_coreMutableStateFlow> levelFlow __attribute__((swift_name("levelFlow")));

/**
 * @note This property has protected visibility in Kotlin source and is intended only for use by subclasses.
*/
@property (readonly) id<EquisenseBluetoothPeripheralWrapperAbstract> peripheral __attribute__((swift_name("peripheral")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ControlCharacteristic")))
@interface EquisenseBluetoothControlCharacteristic : EquisenseBluetoothCharacteristicAbstract
- (instancetype)initWithPeripheral:(id<EquisenseBluetoothPeripheralWrapperAbstract>)peripheral __attribute__((swift_name("init(peripheral:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCharacteristic:(id<EquisenseBluetoothCharacteristic>)characteristic peripheral:(id<EquisenseBluetoothPeripheralWrapperAbstract>)peripheral __attribute__((swift_name("init(characteristic:peripheral:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (EquisenseBluetoothControlCharacteristic *)doCopyPeripheral:(id<EquisenseBluetoothPeripheralWrapperAbstract>)peripheral __attribute__((swift_name("doCopy(peripheral:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (void)onDeviceConnected __attribute__((swift_name("onDeviceConnected()")));
- (void)onReadCharacteristicOpcode:(uint8_t)opcode length:(int32_t)length byteArray:(EquisenseBluetoothKotlinByteArray *)byteArray __attribute__((swift_name("onReadCharacteristic(opcode:length:byteArray:)")));
- (void)readRevision __attribute__((swift_name("readRevision()")));
- (void)readSerial __attribute__((swift_name("readSerial()")));
- (void)revisionUpdatedOnRead:(void (^)(EquisenseBluetoothRevision *))onRead __attribute__((swift_name("revisionUpdated(onRead:)")));
- (void)serialUpdatedOnRead:(void (^)(EquisenseBluetoothSerial *))onRead __attribute__((swift_name("serialUpdated(onRead:)")));
- (NSString *)description __attribute__((swift_name("description()")));

/**
 * @note This property has protected visibility in Kotlin source and is intended only for use by subclasses.
*/
@property (readonly) id<EquisenseBluetoothPeripheralWrapperAbstract> peripheral __attribute__((swift_name("peripheral")));
@end

__attribute__((swift_name("Advertisement")))
@protocol EquisenseBluetoothAdvertisement
@required
- (EquisenseBluetoothKotlinByteArray * _Nullable)manufacturerDataCompanyIdentifierCode:(int32_t)companyIdentifierCode __attribute__((swift_name("manufacturerData(companyIdentifierCode:)")));
- (EquisenseBluetoothKotlinByteArray * _Nullable)serviceDataUuid:(EquisenseBluetoothUuidUuid *)uuid __attribute__((swift_name("serviceData(uuid:)")));
@property (readonly) EquisenseBluetoothBoolean * _Nullable isConnectable __attribute__((swift_name("isConnectable")));
@property (readonly) EquisenseBluetoothManufacturerData * _Nullable manufacturerData __attribute__((swift_name("manufacturerData")));
@property (readonly) NSString * _Nullable name __attribute__((swift_name("name")));
@property (readonly) NSString * _Nullable peripheralName __attribute__((swift_name("peripheralName")));
@property (readonly) int32_t rssi __attribute__((swift_name("rssi")));
@property (readonly) EquisenseBluetoothInt * _Nullable txPower __attribute__((swift_name("txPower")));
@property (readonly) NSArray<EquisenseBluetoothUuidUuid *> *uuids __attribute__((swift_name("uuids")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Bluetooth")))
@interface EquisenseBluetoothBluetooth : EquisenseBluetoothBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)bluetooth __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) EquisenseBluetoothBluetooth *shared __attribute__((swift_name("shared")));
@property (readonly) id<EquisenseBluetoothKotlinx_coroutines_coreFlow> availability __attribute__((swift_name("availability")));
@end

__attribute__((swift_name("Bluetooth.Availability")))
@interface EquisenseBluetoothBluetoothAvailability : EquisenseBluetoothBase
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Bluetooth.AvailabilityAvailable")))
@interface EquisenseBluetoothBluetoothAvailabilityAvailable : EquisenseBluetoothBluetoothAvailability
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)available __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) EquisenseBluetoothBluetoothAvailabilityAvailable *shared __attribute__((swift_name("shared")));
- (NSString *)description __attribute__((swift_name("description()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Bluetooth.AvailabilityUnavailable")))
@interface EquisenseBluetoothBluetoothAvailabilityUnavailable : EquisenseBluetoothBluetoothAvailability
- (instancetype)initWithReason:(EquisenseBluetoothReason * _Nullable)reason __attribute__((swift_name("init(reason:)"))) __attribute__((objc_designated_initializer));
- (EquisenseBluetoothBluetoothAvailabilityUnavailable *)doCopyReason:(EquisenseBluetoothReason * _Nullable)reason __attribute__((swift_name("doCopy(reason:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) EquisenseBluetoothReason * _Nullable reason __attribute__((swift_name("reason")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Bluetooth.BaseUuid")))
@interface EquisenseBluetoothBluetoothBaseUuid : EquisenseBluetoothBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)baseUuid __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) EquisenseBluetoothBluetoothBaseUuid *shared __attribute__((swift_name("shared")));
- (EquisenseBluetoothUuidUuid *)plusShortUuid:(int32_t)shortUuid __attribute__((swift_name("plus(shortUuid:)")));
- (EquisenseBluetoothUuidUuid *)plusShortUuid_:(int64_t)shortUuid __attribute__((swift_name("plus(shortUuid_:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@end

__attribute__((swift_name("KotlinThrowable")))
@interface EquisenseBluetoothKotlinThrowable : EquisenseBluetoothBase
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(EquisenseBluetoothKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(EquisenseBluetoothKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (EquisenseBluetoothKotlinArray<NSString *> *)getStackTrace __attribute__((swift_name("getStackTrace()")));
- (void)printStackTrace __attribute__((swift_name("printStackTrace()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) EquisenseBluetoothKotlinThrowable * _Nullable cause __attribute__((swift_name("cause")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
- (NSError *)asError __attribute__((swift_name("asError()")));
@end

__attribute__((swift_name("KotlinException")))
@interface EquisenseBluetoothKotlinException : EquisenseBluetoothKotlinThrowable
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(EquisenseBluetoothKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(EquisenseBluetoothKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end

__attribute__((swift_name("BluetoothException")))
@interface EquisenseBluetoothBluetoothException : EquisenseBluetoothKotlinException
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(EquisenseBluetoothKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithCause:(EquisenseBluetoothKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BluetoothDisabledException")))
@interface EquisenseBluetoothBluetoothDisabledException : EquisenseBluetoothBluetoothException
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(EquisenseBluetoothKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CentralManager_")))
@interface EquisenseBluetoothCentralManager_ : EquisenseBluetoothBase
@end

__attribute__((swift_name("Characteristic")))
@protocol EquisenseBluetoothCharacteristic
@required
@property (readonly) EquisenseBluetoothUuidUuid *characteristicUuid __attribute__((swift_name("characteristicUuid")));
@property (readonly) EquisenseBluetoothUuidUuid *serviceUuid __attribute__((swift_name("serviceUuid")));
@end

__attribute__((swift_name("IOException")))
@interface EquisenseBluetoothIOException : EquisenseBluetoothKotlinException
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(EquisenseBluetoothKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithCause:(EquisenseBluetoothKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end

__attribute__((swift_name("NotConnectedException")))
@interface EquisenseBluetoothNotConnectedException : EquisenseBluetoothIOException
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(EquisenseBluetoothKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConnectionLostException")))
@interface EquisenseBluetoothConnectionLostException : EquisenseBluetoothNotConnectedException
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(EquisenseBluetoothKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ConnectionRejectedException")))
@interface EquisenseBluetoothConnectionRejectedException : EquisenseBluetoothIOException
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(EquisenseBluetoothKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
@end

__attribute__((swift_name("CoreBluetoothAdvertisement")))
@protocol EquisenseBluetoothCoreBluetoothAdvertisement <EquisenseBluetoothAdvertisement>
@required
- (NSData * _Nullable)manufacturerDataAsNSDataCompanyIdentifierCode:(int32_t)companyIdentifierCode __attribute__((swift_name("manufacturerDataAsNSData(companyIdentifierCode:)")));
- (NSData * _Nullable)serviceDataAsNSDataUuid:(EquisenseBluetoothUuidUuid *)uuid __attribute__((swift_name("serviceDataAsNSData(uuid:)")));
@property (readonly) NSUUID *identifier __attribute__((swift_name("identifier")));
@property (readonly) NSData * _Nullable manufacturerDataAsNSData __attribute__((swift_name("manufacturerDataAsNSData")));
@end

__attribute__((swift_name("Peripheral")))
@protocol EquisenseBluetoothPeripheral
@required

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)connectWithCompletionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("connect(completionHandler:)")));

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)disconnectWithCompletionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("disconnect(completionHandler:)")));
- (id<EquisenseBluetoothKotlinx_coroutines_coreFlow>)observeCharacteristic:(id<EquisenseBluetoothCharacteristic>)characteristic onSubscription:(id<EquisenseBluetoothKotlinSuspendFunction0>)onSubscription __attribute__((swift_name("observe(characteristic:onSubscription:)")));

/**
 * @note This method converts instances of CancellationException, IOException, NotReadyException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)readCharacteristic:(id<EquisenseBluetoothCharacteristic>)characteristic completionHandler:(void (^)(EquisenseBluetoothKotlinByteArray * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("read(characteristic:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException, IOException, NotReadyException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)readDescriptor:(id<EquisenseBluetoothDescriptor>)descriptor completionHandler:(void (^)(EquisenseBluetoothKotlinByteArray * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("read(descriptor:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException, IOException, NotReadyException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)rssiWithCompletionHandler:(void (^)(EquisenseBluetoothInt * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("rssi(completionHandler:)")));

/**
 * @note This method converts instances of CancellationException, IOException, NotReadyException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)writeCharacteristic:(id<EquisenseBluetoothCharacteristic>)characteristic data:(EquisenseBluetoothKotlinByteArray *)data writeType:(EquisenseBluetoothWriteType *)writeType completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("write(characteristic:data:writeType:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException, IOException, NotReadyException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)writeDescriptor:(id<EquisenseBluetoothDescriptor>)descriptor data:(EquisenseBluetoothKotlinByteArray *)data completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("write(descriptor:data:completionHandler:)")));
@property (readonly) NSString * _Nullable name __attribute__((swift_name("name")));
@property (readonly) NSArray<EquisenseBluetoothDiscoveredService *> * _Nullable services __attribute__((swift_name("services")));
@property (readonly) id<EquisenseBluetoothKotlinx_coroutines_coreStateFlow> state __attribute__((swift_name("state")));
@end

__attribute__((swift_name("CoreBluetoothPeripheral")))
@protocol EquisenseBluetoothCoreBluetoothPeripheral <EquisenseBluetoothPeripheral>
@required
- (id<EquisenseBluetoothKotlinx_coroutines_coreFlow>)observeAsNSDataCharacteristic:(id<EquisenseBluetoothCharacteristic>)characteristic onSubscription:(id<EquisenseBluetoothKotlinSuspendFunction0>)onSubscription __attribute__((swift_name("observeAsNSData(characteristic:onSubscription:)")));

/**
 * @note This method converts instances of CancellationException, IOException, NotReadyException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)readAsNSDataCharacteristic:(id<EquisenseBluetoothCharacteristic>)characteristic completionHandler:(void (^)(NSData * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readAsNSData(characteristic:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException, IOException, NotReadyException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)readAsNSDataDescriptor:(id<EquisenseBluetoothDescriptor>)descriptor completionHandler:(void (^)(NSData * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readAsNSData(descriptor:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException, IOException, NotReadyException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)writeCharacteristic:(id<EquisenseBluetoothCharacteristic>)characteristic data:(NSData *)data writeType:(EquisenseBluetoothWriteType *)writeType completionHandler_:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("write(characteristic:data:writeType:completionHandler_:)")));

/**
 * @note This method converts instances of CancellationException, IOException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)writeDescriptor:(id<EquisenseBluetoothDescriptor>)descriptor data:(NSData *)data completionHandler_:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("write(descriptor:data:completionHandler_:)")));
@end

__attribute__((swift_name("Scanner")))
@protocol EquisenseBluetoothScanner
@required
@property (readonly) id<EquisenseBluetoothKotlinx_coroutines_coreFlow> advertisements __attribute__((swift_name("advertisements")));
@end

__attribute__((swift_name("CoreBluetoothScanner")))
@protocol EquisenseBluetoothCoreBluetoothScanner <EquisenseBluetoothScanner>
@required
@end

__attribute__((swift_name("Descriptor")))
@protocol EquisenseBluetoothDescriptor
@required
@property (readonly) EquisenseBluetoothUuidUuid *characteristicUuid __attribute__((swift_name("characteristicUuid")));
@property (readonly) EquisenseBluetoothUuidUuid *descriptorUuid __attribute__((swift_name("descriptorUuid")));
@property (readonly) EquisenseBluetoothUuidUuid *serviceUuid __attribute__((swift_name("serviceUuid")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DiscoveredCharacteristic")))
@interface EquisenseBluetoothDiscoveredCharacteristic : EquisenseBluetoothBase <EquisenseBluetoothCharacteristic>
- (EquisenseBluetoothDiscoveredCharacteristic *)doCopyCharacteristic:(CBCharacteristic *)characteristic __attribute__((swift_name("doCopy(characteristic:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) EquisenseBluetoothUuidUuid *characteristicUuid __attribute__((swift_name("characteristicUuid")));
@property (readonly) NSArray<EquisenseBluetoothDiscoveredDescriptor *> *descriptors __attribute__((swift_name("descriptors")));
@property (readonly) int32_t properties __attribute__((swift_name("properties")));
@property (readonly) EquisenseBluetoothUuidUuid *serviceUuid __attribute__((swift_name("serviceUuid")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DiscoveredDescriptor")))
@interface EquisenseBluetoothDiscoveredDescriptor : EquisenseBluetoothBase <EquisenseBluetoothDescriptor>
- (EquisenseBluetoothDiscoveredDescriptor *)doCopyDescriptor:(CBDescriptor *)descriptor __attribute__((swift_name("doCopy(descriptor:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) EquisenseBluetoothUuidUuid *characteristicUuid __attribute__((swift_name("characteristicUuid")));
@property (readonly) EquisenseBluetoothUuidUuid *descriptorUuid __attribute__((swift_name("descriptorUuid")));
@property (readonly) EquisenseBluetoothUuidUuid *serviceUuid __attribute__((swift_name("serviceUuid")));
@end

__attribute__((swift_name("Service_")))
@protocol EquisenseBluetoothService
@required
@property (readonly) EquisenseBluetoothUuidUuid *serviceUuid __attribute__((swift_name("serviceUuid")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("DiscoveredService")))
@interface EquisenseBluetoothDiscoveredService : EquisenseBluetoothBase <EquisenseBluetoothService>
- (EquisenseBluetoothDiscoveredService *)doCopyService:(CBService *)service __attribute__((swift_name("doCopy(service:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSArray<EquisenseBluetoothDiscoveredCharacteristic *> *characteristics __attribute__((swift_name("characteristics")));
@property (readonly) EquisenseBluetoothUuidUuid *serviceUuid __attribute__((swift_name("serviceUuid")));
@end

__attribute__((swift_name("Filter")))
@interface EquisenseBluetoothFilter : EquisenseBluetoothBase
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Filter.Address")))
@interface EquisenseBluetoothFilterAddress : EquisenseBluetoothFilter
- (instancetype)initWithAddress:(NSString *)address __attribute__((swift_name("init(address:)"))) __attribute__((objc_designated_initializer));
- (EquisenseBluetoothFilterAddress *)doCopyAddress:(NSString *)address __attribute__((swift_name("doCopy(address:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *address __attribute__((swift_name("address")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Filter.ManufacturerData")))
@interface EquisenseBluetoothFilterManufacturerData : EquisenseBluetoothFilter
- (instancetype)initWithId:(EquisenseBluetoothKotlinByteArray *)id data:(EquisenseBluetoothKotlinByteArray *)data dataMask:(EquisenseBluetoothKotlinByteArray * _Nullable)dataMask __attribute__((swift_name("init(id:data:dataMask:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithId:(int32_t)id data:(EquisenseBluetoothKotlinByteArray *)data dataMask_:(EquisenseBluetoothKotlinByteArray * _Nullable)dataMask __attribute__((swift_name("init(id:data:dataMask_:)"))) __attribute__((objc_designated_initializer));
@property (readonly) EquisenseBluetoothKotlinByteArray *data __attribute__((swift_name("data")));
@property (readonly) EquisenseBluetoothKotlinByteArray * _Nullable dataMask __attribute__((swift_name("dataMask")));
@property (readonly) int32_t id __attribute__((swift_name("id")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Filter.Name")))
@interface EquisenseBluetoothFilterName : EquisenseBluetoothFilter
- (instancetype)initWithName:(NSString *)name __attribute__((swift_name("init(name:)"))) __attribute__((objc_designated_initializer));
- (EquisenseBluetoothFilterName *)doCopyName:(NSString *)name __attribute__((swift_name("doCopy(name:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Filter.NamePrefix")))
@interface EquisenseBluetoothFilterNamePrefix : EquisenseBluetoothFilter
- (instancetype)initWithPrefix:(NSString *)prefix __attribute__((swift_name("init(prefix:)"))) __attribute__((objc_designated_initializer));
- (EquisenseBluetoothFilterNamePrefix *)doCopyPrefix:(NSString *)prefix __attribute__((swift_name("doCopy(prefix:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *prefix __attribute__((swift_name("prefix")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Filter.Service")))
@interface EquisenseBluetoothFilterService : EquisenseBluetoothFilter
- (instancetype)initWithUuid:(EquisenseBluetoothUuidUuid *)uuid __attribute__((swift_name("init(uuid:)"))) __attribute__((objc_designated_initializer));
- (EquisenseBluetoothFilterService *)doCopyUuid:(EquisenseBluetoothUuidUuid *)uuid __attribute__((swift_name("doCopy(uuid:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) EquisenseBluetoothUuidUuid *uuid __attribute__((swift_name("uuid")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("GattStatusException")))
@interface EquisenseBluetoothGattStatusException : EquisenseBluetoothIOException
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(EquisenseBluetoothKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LazyCharacteristic")))
@interface EquisenseBluetoothLazyCharacteristic : EquisenseBluetoothBase <EquisenseBluetoothCharacteristic>
- (EquisenseBluetoothLazyCharacteristic *)doCopyServiceUuid:(EquisenseBluetoothUuidUuid *)serviceUuid characteristicUuid:(EquisenseBluetoothUuidUuid *)characteristicUuid __attribute__((swift_name("doCopy(serviceUuid:characteristicUuid:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) EquisenseBluetoothUuidUuid *characteristicUuid __attribute__((swift_name("characteristicUuid")));
@property (readonly) EquisenseBluetoothUuidUuid *serviceUuid __attribute__((swift_name("serviceUuid")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("LazyDescriptor")))
@interface EquisenseBluetoothLazyDescriptor : EquisenseBluetoothBase <EquisenseBluetoothDescriptor>
- (instancetype)initWithServiceUuid:(EquisenseBluetoothUuidUuid *)serviceUuid characteristicUuid:(EquisenseBluetoothUuidUuid *)characteristicUuid descriptorUuid:(EquisenseBluetoothUuidUuid *)descriptorUuid __attribute__((swift_name("init(serviceUuid:characteristicUuid:descriptorUuid:)"))) __attribute__((objc_designated_initializer));
- (EquisenseBluetoothLazyDescriptor *)doCopyServiceUuid:(EquisenseBluetoothUuidUuid *)serviceUuid characteristicUuid:(EquisenseBluetoothUuidUuid *)characteristicUuid descriptorUuid:(EquisenseBluetoothUuidUuid *)descriptorUuid __attribute__((swift_name("doCopy(serviceUuid:characteristicUuid:descriptorUuid:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) EquisenseBluetoothUuidUuid *characteristicUuid __attribute__((swift_name("characteristicUuid")));
@property (readonly) EquisenseBluetoothUuidUuid *descriptorUuid __attribute__((swift_name("descriptorUuid")));
@property (readonly) EquisenseBluetoothUuidUuid *serviceUuid __attribute__((swift_name("serviceUuid")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ManufacturerData")))
@interface EquisenseBluetoothManufacturerData : EquisenseBluetoothBase
- (instancetype)initWithCode:(int32_t)code data:(EquisenseBluetoothKotlinByteArray *)data __attribute__((swift_name("init(code:data:)"))) __attribute__((objc_designated_initializer));
@property (readonly) int32_t code __attribute__((swift_name("code")));
@property (readonly) EquisenseBluetoothKotlinByteArray *data __attribute__((swift_name("data")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("NotReadyException")))
@interface EquisenseBluetoothNotReadyException : EquisenseBluetoothNotConnectedException
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(EquisenseBluetoothKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ObservationExceptionPeripheral")))
@interface EquisenseBluetoothObservationExceptionPeripheral : EquisenseBluetoothBase
@property (readonly) id<EquisenseBluetoothKotlinx_coroutines_coreStateFlow> state __attribute__((swift_name("state")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PeripheralBuilder")))
@interface EquisenseBluetoothPeripheralBuilder : EquisenseBluetoothBase
- (void)loggingInit:(void (^)(EquisenseBluetoothLogging *))init __attribute__((swift_name("logging(init:)")));
- (void)observationExceptionHandlerHandler:(id<EquisenseBluetoothKotlinSuspendFunction2>)handler __attribute__((swift_name("observationExceptionHandler(handler:)")));
- (void)onServicesDiscoveredAction:(id<EquisenseBluetoothKotlinSuspendFunction1>)action __attribute__((swift_name("onServicesDiscovered(action:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Reason")))
@interface EquisenseBluetoothReason : EquisenseBluetoothKotlinEnum<EquisenseBluetoothReason *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) EquisenseBluetoothReason *off __attribute__((swift_name("off")));
@property (class, readonly) EquisenseBluetoothReason *resetting __attribute__((swift_name("resetting")));
@property (class, readonly) EquisenseBluetoothReason *unauthorized __attribute__((swift_name("unauthorized")));
@property (class, readonly) EquisenseBluetoothReason *unsupported __attribute__((swift_name("unsupported")));
@property (class, readonly) EquisenseBluetoothReason *unknown __attribute__((swift_name("unknown")));
+ (EquisenseBluetoothKotlinArray<EquisenseBluetoothReason *> *)values __attribute__((swift_name("values()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ScannerBuilder")))
@interface EquisenseBluetoothScannerBuilder : EquisenseBluetoothBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)loggingInit:(void (^)(EquisenseBluetoothLogging *))init __attribute__((swift_name("logging(init:)")));
@property EquisenseBluetoothBoolean * _Nullable allowDuplicateKeys __attribute__((swift_name("allowDuplicateKeys")));
@property NSArray<EquisenseBluetoothFilter *> * _Nullable filters __attribute__((swift_name("filters")));
@property NSArray<EquisenseBluetoothUuidUuid *> * _Nullable solicitedServiceUuids __attribute__((swift_name("solicitedServiceUuids")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ServicesDiscoveredPeripheral")))
@interface EquisenseBluetoothServicesDiscoveredPeripheral : EquisenseBluetoothBase

/**
 * @note This method converts instances of CancellationException, IOException, NotReadyException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)readCharacteristic:(id<EquisenseBluetoothCharacteristic>)characteristic completionHandler:(void (^)(EquisenseBluetoothKotlinByteArray * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("read(characteristic:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException, IOException, NotReadyException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)readDescriptor:(id<EquisenseBluetoothDescriptor>)descriptor completionHandler:(void (^)(EquisenseBluetoothKotlinByteArray * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("read(descriptor:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException, IOException, NotReadyException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)writeCharacteristic:(id<EquisenseBluetoothCharacteristic>)characteristic data:(EquisenseBluetoothKotlinByteArray *)data writeType:(EquisenseBluetoothWriteType *)writeType completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("write(characteristic:data:writeType:completionHandler:)")));

/**
 * @note This method converts instances of CancellationException, IOException, NotReadyException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)writeDescriptor:(id<EquisenseBluetoothDescriptor>)descriptor data:(EquisenseBluetoothKotlinByteArray *)data completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("write(descriptor:data:completionHandler:)")));
@end

__attribute__((swift_name("State")))
@interface EquisenseBluetoothState : EquisenseBluetoothBase
- (NSString *)description __attribute__((swift_name("description()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("State.Connected")))
@interface EquisenseBluetoothStateConnected : EquisenseBluetoothState
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)connected __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) EquisenseBluetoothStateConnected *shared __attribute__((swift_name("shared")));
@end

__attribute__((swift_name("State.Connecting")))
@interface EquisenseBluetoothStateConnecting : EquisenseBluetoothState
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("State.ConnectingBluetooth")))
@interface EquisenseBluetoothStateConnectingBluetooth : EquisenseBluetoothStateConnecting
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)bluetooth __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) EquisenseBluetoothStateConnectingBluetooth *shared __attribute__((swift_name("shared")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("State.ConnectingObserves")))
@interface EquisenseBluetoothStateConnectingObserves : EquisenseBluetoothStateConnecting
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)observes __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) EquisenseBluetoothStateConnectingObserves *shared __attribute__((swift_name("shared")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("State.ConnectingServices")))
@interface EquisenseBluetoothStateConnectingServices : EquisenseBluetoothStateConnecting
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)services __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) EquisenseBluetoothStateConnectingServices *shared __attribute__((swift_name("shared")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("State.Disconnected")))
@interface EquisenseBluetoothStateDisconnected : EquisenseBluetoothState
- (instancetype)initWithStatus:(EquisenseBluetoothStateDisconnectedStatus * _Nullable)status __attribute__((swift_name("init(status:)"))) __attribute__((objc_designated_initializer));
- (EquisenseBluetoothStateDisconnected *)doCopyStatus:(EquisenseBluetoothStateDisconnectedStatus * _Nullable)status __attribute__((swift_name("doCopy(status:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) EquisenseBluetoothStateDisconnectedStatus * _Nullable status __attribute__((swift_name("status")));
@end

__attribute__((swift_name("State.DisconnectedStatus")))
@interface EquisenseBluetoothStateDisconnectedStatus : EquisenseBluetoothBase
- (NSString *)description __attribute__((swift_name("description()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("State.DisconnectedStatusCancelled")))
@interface EquisenseBluetoothStateDisconnectedStatusCancelled : EquisenseBluetoothStateDisconnectedStatus
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)cancelled __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) EquisenseBluetoothStateDisconnectedStatusCancelled *shared __attribute__((swift_name("shared")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("State.DisconnectedStatusCentralDisconnected")))
@interface EquisenseBluetoothStateDisconnectedStatusCentralDisconnected : EquisenseBluetoothStateDisconnectedStatus
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)centralDisconnected __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) EquisenseBluetoothStateDisconnectedStatusCentralDisconnected *shared __attribute__((swift_name("shared")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("State.DisconnectedStatusConnectionLimitReached")))
@interface EquisenseBluetoothStateDisconnectedStatusConnectionLimitReached : EquisenseBluetoothStateDisconnectedStatus
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)connectionLimitReached __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) EquisenseBluetoothStateDisconnectedStatusConnectionLimitReached *shared __attribute__((swift_name("shared")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("State.DisconnectedStatusEncryptionTimedOut")))
@interface EquisenseBluetoothStateDisconnectedStatusEncryptionTimedOut : EquisenseBluetoothStateDisconnectedStatus
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)encryptionTimedOut __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) EquisenseBluetoothStateDisconnectedStatusEncryptionTimedOut *shared __attribute__((swift_name("shared")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("State.DisconnectedStatusFailed")))
@interface EquisenseBluetoothStateDisconnectedStatusFailed : EquisenseBluetoothStateDisconnectedStatus
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)failed __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) EquisenseBluetoothStateDisconnectedStatusFailed *shared __attribute__((swift_name("shared")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("State.DisconnectedStatusL2CapFailure")))
@interface EquisenseBluetoothStateDisconnectedStatusL2CapFailure : EquisenseBluetoothStateDisconnectedStatus
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)l2CapFailure __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) EquisenseBluetoothStateDisconnectedStatusL2CapFailure *shared __attribute__((swift_name("shared")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("State.DisconnectedStatusLinkManagerProtocolTimeout")))
@interface EquisenseBluetoothStateDisconnectedStatusLinkManagerProtocolTimeout : EquisenseBluetoothStateDisconnectedStatus
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)linkManagerProtocolTimeout __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) EquisenseBluetoothStateDisconnectedStatusLinkManagerProtocolTimeout *shared __attribute__((swift_name("shared")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("State.DisconnectedStatusPeripheralDisconnected")))
@interface EquisenseBluetoothStateDisconnectedStatusPeripheralDisconnected : EquisenseBluetoothStateDisconnectedStatus
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)peripheralDisconnected __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) EquisenseBluetoothStateDisconnectedStatusPeripheralDisconnected *shared __attribute__((swift_name("shared")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("State.DisconnectedStatusTimeout")))
@interface EquisenseBluetoothStateDisconnectedStatusTimeout : EquisenseBluetoothStateDisconnectedStatus
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)timeout __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) EquisenseBluetoothStateDisconnectedStatusTimeout *shared __attribute__((swift_name("shared")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("State.DisconnectedStatusUnknown")))
@interface EquisenseBluetoothStateDisconnectedStatusUnknown : EquisenseBluetoothStateDisconnectedStatus
- (instancetype)initWithStatus:(int32_t)status __attribute__((swift_name("init(status:)"))) __attribute__((objc_designated_initializer));
- (EquisenseBluetoothStateDisconnectedStatusUnknown *)doCopyStatus:(int32_t)status __attribute__((swift_name("doCopy(status:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t status __attribute__((swift_name("status")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("State.DisconnectedStatusUnknownDevice")))
@interface EquisenseBluetoothStateDisconnectedStatusUnknownDevice : EquisenseBluetoothStateDisconnectedStatus
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)unknownDevice __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) EquisenseBluetoothStateDisconnectedStatusUnknownDevice *shared __attribute__((swift_name("shared")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("State.Disconnecting")))
@interface EquisenseBluetoothStateDisconnecting : EquisenseBluetoothState
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)disconnecting __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) EquisenseBluetoothStateDisconnecting *shared __attribute__((swift_name("shared")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("WriteType")))
@interface EquisenseBluetoothWriteType : EquisenseBluetoothKotlinEnum<EquisenseBluetoothWriteType *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) EquisenseBluetoothWriteType *withresponse __attribute__((swift_name("withresponse")));
@property (class, readonly) EquisenseBluetoothWriteType *withoutresponse __attribute__((swift_name("withoutresponse")));
+ (EquisenseBluetoothKotlinArray<EquisenseBluetoothWriteType *> *)values __attribute__((swift_name("values()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("HexBuilder")))
@interface EquisenseBluetoothHexBuilder : EquisenseBluetoothBase
@property BOOL lowerCase __attribute__((swift_name("lowerCase")));
@property NSString *separator __attribute__((swift_name("separator")));
@end

__attribute__((swift_name("LogEngine")))
@protocol EquisenseBluetoothLogEngine
@required
- (void)assertThrowable:(EquisenseBluetoothKotlinThrowable * _Nullable)throwable tag:(NSString *)tag message:(NSString *)message __attribute__((swift_name("assert(throwable:tag:message:)")));
- (void)debugThrowable:(EquisenseBluetoothKotlinThrowable * _Nullable)throwable tag:(NSString *)tag message:(NSString *)message __attribute__((swift_name("debug(throwable:tag:message:)")));
- (void)errorThrowable:(EquisenseBluetoothKotlinThrowable * _Nullable)throwable tag:(NSString *)tag message:(NSString *)message __attribute__((swift_name("error(throwable:tag:message:)")));
- (void)infoThrowable:(EquisenseBluetoothKotlinThrowable * _Nullable)throwable tag:(NSString *)tag message:(NSString *)message __attribute__((swift_name("info(throwable:tag:message:)")));
- (void)verboseThrowable:(EquisenseBluetoothKotlinThrowable * _Nullable)throwable tag:(NSString *)tag message:(NSString *)message __attribute__((swift_name("verbose(throwable:tag:message:)")));
- (void)warnThrowable:(EquisenseBluetoothKotlinThrowable * _Nullable)throwable tag:(NSString *)tag message:(NSString *)message __attribute__((swift_name("warn(throwable:tag:message:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Logging")))
@interface EquisenseBluetoothLogging : EquisenseBluetoothBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property id<EquisenseBluetoothLoggingDataProcessor> data __attribute__((swift_name("data")));
@property id<EquisenseBluetoothLogEngine> engine __attribute__((swift_name("engine")));
@property EquisenseBluetoothLoggingFormat *format __attribute__((swift_name("format")));
@property NSString * _Nullable identifier __attribute__((swift_name("identifier")));
@property EquisenseBluetoothLoggingLevel *level __attribute__((swift_name("level")));
@end

__attribute__((swift_name("LoggingDataProcessor")))
@protocol EquisenseBluetoothLoggingDataProcessor
@required
- (NSString *)processData:(EquisenseBluetoothKotlinByteArray *)data __attribute__((swift_name("process(data:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Logging.Format")))
@interface EquisenseBluetoothLoggingFormat : EquisenseBluetoothKotlinEnum<EquisenseBluetoothLoggingFormat *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) EquisenseBluetoothLoggingFormat *compact __attribute__((swift_name("compact")));
@property (class, readonly) EquisenseBluetoothLoggingFormat *multiline __attribute__((swift_name("multiline")));
+ (EquisenseBluetoothKotlinArray<EquisenseBluetoothLoggingFormat *> *)values __attribute__((swift_name("values()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Logging.Level")))
@interface EquisenseBluetoothLoggingLevel : EquisenseBluetoothKotlinEnum<EquisenseBluetoothLoggingLevel *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) EquisenseBluetoothLoggingLevel *warnings __attribute__((swift_name("warnings")));
@property (class, readonly) EquisenseBluetoothLoggingLevel *events __attribute__((swift_name("events")));
@property (class, readonly) EquisenseBluetoothLoggingLevel *data __attribute__((swift_name("data")));
+ (EquisenseBluetoothKotlinArray<EquisenseBluetoothLoggingLevel *> *)values __attribute__((swift_name("values()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SystemLogEngine")))
@interface EquisenseBluetoothSystemLogEngine : EquisenseBluetoothBase <EquisenseBluetoothLogEngine>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)systemLogEngine __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) EquisenseBluetoothSystemLogEngine *shared __attribute__((swift_name("shared")));
- (void)assertThrowable:(EquisenseBluetoothKotlinThrowable * _Nullable)throwable tag:(NSString *)tag message:(NSString *)message __attribute__((swift_name("assert(throwable:tag:message:)")));
- (void)debugThrowable:(EquisenseBluetoothKotlinThrowable * _Nullable)throwable tag:(NSString *)tag message:(NSString *)message __attribute__((swift_name("debug(throwable:tag:message:)")));
- (void)errorThrowable:(EquisenseBluetoothKotlinThrowable * _Nullable)throwable tag:(NSString *)tag message:(NSString *)message __attribute__((swift_name("error(throwable:tag:message:)")));
- (void)infoThrowable:(EquisenseBluetoothKotlinThrowable * _Nullable)throwable tag:(NSString *)tag message:(NSString *)message __attribute__((swift_name("info(throwable:tag:message:)")));
- (void)verboseThrowable:(EquisenseBluetoothKotlinThrowable * _Nullable)throwable tag:(NSString *)tag message:(NSString *)message __attribute__((swift_name("verbose(throwable:tag:message:)")));
- (void)warnThrowable:(EquisenseBluetoothKotlinThrowable * _Nullable)throwable tag:(NSString *)tag message:(NSString *)message __attribute__((swift_name("warn(throwable:tag:message:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinByteArray")))
@interface EquisenseBluetoothKotlinByteArray : EquisenseBluetoothBase
+ (instancetype)arrayWithSize:(int32_t)size __attribute__((swift_name("init(size:)")));
+ (instancetype)arrayWithSize:(int32_t)size init:(EquisenseBluetoothByte *(^)(EquisenseBluetoothInt *))init __attribute__((swift_name("init(size:init:)")));
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (int8_t)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (EquisenseBluetoothKotlinByteIterator *)iterator __attribute__((swift_name("iterator()")));
- (void)setIndex:(int32_t)index value:(int8_t)value __attribute__((swift_name("set(index:value:)")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end

@interface EquisenseBluetoothKotlinByteArray (Extensions)
- (NSString *)toHexString __attribute__((swift_name("toHexString()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("CentralManagerKt")))
@interface EquisenseBluetoothCentralManagerKt : EquisenseBluetoothBase
+ (EquisenseBluetoothCentralManager *)getCentralManagerDelegate:(id<EquisenseBluetoothCentralManagerDelegate>)delegate __attribute__((swift_name("getCentralManager(delegate:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ControlKt")))
@interface EquisenseBluetoothControlKt : EquisenseBluetoothBase
+ (uint64_t)littleEndianConversionBytes:(EquisenseBluetoothKotlinByteArray *)bytes __attribute__((swift_name("littleEndianConversion(bytes:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ProfileKt")))
@interface EquisenseBluetoothProfileKt : EquisenseBluetoothBase
+ (BOOL)broadcast:(int32_t)receiver __attribute__((swift_name("broadcast(_:)")));
+ (BOOL)extendedProperties:(int32_t)receiver __attribute__((swift_name("extendedProperties(_:)")));
+ (BOOL)indicate:(int32_t)receiver __attribute__((swift_name("indicate(_:)")));
+ (BOOL)notify:(int32_t)receiver __attribute__((swift_name("notify(_:)")));
+ (BOOL)read:(int32_t)receiver __attribute__((swift_name("read(_:)")));
+ (BOOL)signedWrite:(int32_t)receiver __attribute__((swift_name("signedWrite(_:)")));
+ (BOOL)write:(int32_t)receiver __attribute__((swift_name("write(_:)")));
+ (BOOL)writeWithoutResponse:(int32_t)receiver __attribute__((swift_name("writeWithoutResponse(_:)")));
+ (id<EquisenseBluetoothCharacteristic>)characteristicOfService:(NSString *)service characteristic:(NSString *)characteristic __attribute__((swift_name("characteristicOf(service:characteristic:)")));
+ (id<EquisenseBluetoothDescriptor>)descriptorOfService:(NSString *)service characteristic:(NSString *)characteristic descriptor:(NSString *)descriptor __attribute__((swift_name("descriptorOf(service:characteristic:descriptor:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IdentifierKt")))
@interface EquisenseBluetoothIdentifierKt : EquisenseBluetoothBase
+ (EquisenseBluetoothUuidUuid *)identifier:(id<EquisenseBluetoothPeripheral>)receiver __attribute__((swift_name("identifier(_:)")));
+ (EquisenseBluetoothUuidUuid *)toIdentifier:(NSString *)receiver __attribute__((swift_name("toIdentifier(_:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ScannerKt")))
@interface EquisenseBluetoothScannerKt : EquisenseBluetoothBase
+ (id<EquisenseBluetoothCoreBluetoothScanner>)ScannerBuilderAction:(void (^)(EquisenseBluetoothScannerBuilder *))builderAction __attribute__((swift_name("Scanner(builderAction:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PeripheralKt")))
@interface EquisenseBluetoothPeripheralKt : EquisenseBluetoothBase
+ (id<EquisenseBluetoothPeripheral>)peripheral:(id<EquisenseBluetoothKotlinx_coroutines_coreCoroutineScope>)receiver advertisement:(id<EquisenseBluetoothAdvertisement>)advertisement builderAction:(void (^)(EquisenseBluetoothPeripheralBuilder *))builderAction __attribute__((swift_name("peripheral(_:advertisement:builderAction:)")));
+ (id<EquisenseBluetoothPeripheral>)peripheral:(id<EquisenseBluetoothKotlinx_coroutines_coreCoroutineScope>)receiver identifier:(EquisenseBluetoothUuidUuid *)identifier builderAction:(void (^)(EquisenseBluetoothPeripheralBuilder *))builderAction __attribute__((swift_name("peripheral(_:identifier:builderAction:)")));
+ (id<EquisenseBluetoothCoreBluetoothPeripheral>)peripheral:(id<EquisenseBluetoothKotlinx_coroutines_coreCoroutineScope>)receiver cbPeripheral:(CBPeripheral *)cbPeripheral builderAction:(void (^)(EquisenseBluetoothPeripheralBuilder *))builderAction __attribute__((swift_name("peripheral(_:cbPeripheral:builderAction:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("HexKt")))
@interface EquisenseBluetoothHexKt : EquisenseBluetoothBase
+ (id<EquisenseBluetoothLoggingDataProcessor>)HexInit:(void (^)(EquisenseBluetoothHexBuilder *))init __attribute__((swift_name("Hex(init:)")));
@property (class, readonly) id<EquisenseBluetoothLoggingDataProcessor> Hex __attribute__((swift_name("Hex")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("UuidUuid")))
@interface EquisenseBluetoothUuidUuid : EquisenseBluetoothBase <EquisenseBluetoothKotlinComparable>
- (instancetype)initWithMsb:(int64_t)msb lsb:(int64_t)lsb __attribute__((swift_name("init(msb:lsb:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithUuidBytes:(EquisenseBluetoothKotlinByteArray *)uuidBytes __attribute__((swift_name("init(uuidBytes:)"))) __attribute__((objc_designated_initializer)) __attribute__((deprecated("Use `uuidOf` instead.")));
- (int32_t)compareToOther:(EquisenseBluetoothUuidUuid *)other __attribute__((swift_name("compareTo(other:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int64_t leastSignificantBits __attribute__((swift_name("leastSignificantBits")));
@property (readonly) int64_t mostSignificantBits __attribute__((swift_name("mostSignificantBits")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinEnumCompanion")))
@interface EquisenseBluetoothKotlinEnumCompanion : EquisenseBluetoothBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) EquisenseBluetoothKotlinEnumCompanion *shared __attribute__((swift_name("shared")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinArray")))
@interface EquisenseBluetoothKotlinArray<T> : EquisenseBluetoothBase
+ (instancetype)arrayWithSize:(int32_t)size init:(T _Nullable (^)(EquisenseBluetoothInt *))init __attribute__((swift_name("init(size:init:)")));
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (T _Nullable)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (id<EquisenseBluetoothKotlinIterator>)iterator __attribute__((swift_name("iterator()")));
- (void)setIndex:(int32_t)index value:(T _Nullable)value __attribute__((swift_name("set(index:value:)")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end

__attribute__((swift_name("KotlinRuntimeException")))
@interface EquisenseBluetoothKotlinRuntimeException : EquisenseBluetoothKotlinException
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(EquisenseBluetoothKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(EquisenseBluetoothKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end

__attribute__((swift_name("KotlinIllegalStateException")))
@interface EquisenseBluetoothKotlinIllegalStateException : EquisenseBluetoothKotlinRuntimeException
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(EquisenseBluetoothKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(EquisenseBluetoothKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end


/**
 * @note annotations
 *   kotlin.SinceKotlin(version="1.4")
*/
__attribute__((swift_name("KotlinCancellationException")))
@interface EquisenseBluetoothKotlinCancellationException : EquisenseBluetoothKotlinIllegalStateException
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(EquisenseBluetoothKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(EquisenseBluetoothKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end

__attribute__((swift_name("KotlinFunction")))
@protocol EquisenseBluetoothKotlinFunction
@required
@end

__attribute__((swift_name("KotlinSuspendFunction0")))
@protocol EquisenseBluetoothKotlinSuspendFunction0 <EquisenseBluetoothKotlinFunction>
@required

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)invokeWithCompletionHandler:(void (^)(id _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("invoke(completionHandler:)")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_datetimeInstant")))
@interface EquisenseBluetoothKotlinx_datetimeInstant : EquisenseBluetoothBase <EquisenseBluetoothKotlinComparable>
@property (class, readonly, getter=companion) EquisenseBluetoothKotlinx_datetimeInstantCompanion *companion __attribute__((swift_name("companion")));
- (int32_t)compareToOther:(EquisenseBluetoothKotlinx_datetimeInstant *)other __attribute__((swift_name("compareTo(other:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (EquisenseBluetoothKotlinx_datetimeInstant *)minusDuration:(int64_t)duration __attribute__((swift_name("minus(duration:)")));
- (int64_t)minusOther:(EquisenseBluetoothKotlinx_datetimeInstant *)other __attribute__((swift_name("minus(other:)")));
- (EquisenseBluetoothKotlinx_datetimeInstant *)plusDuration:(int64_t)duration __attribute__((swift_name("plus(duration:)")));
- (int64_t)toEpochMilliseconds __attribute__((swift_name("toEpochMilliseconds()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int64_t epochSeconds __attribute__((swift_name("epochSeconds")));
@property (readonly) int32_t nanosecondsOfSecond __attribute__((swift_name("nanosecondsOfSecond")));
@end

__attribute__((swift_name("Kotlinx_coroutines_coreCoroutineScope")))
@protocol EquisenseBluetoothKotlinx_coroutines_coreCoroutineScope
@required
@property (readonly) id<EquisenseBluetoothKotlinCoroutineContext> coroutineContext __attribute__((swift_name("coroutineContext")));
@end

__attribute__((swift_name("Kotlinx_coroutines_coreFlow")))
@protocol EquisenseBluetoothKotlinx_coroutines_coreFlow
@required

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)collectCollector:(id<EquisenseBluetoothKotlinx_coroutines_coreFlowCollector>)collector completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("collect(collector:completionHandler:)")));
@end

__attribute__((swift_name("Kotlinx_coroutines_coreSharedFlow")))
@protocol EquisenseBluetoothKotlinx_coroutines_coreSharedFlow <EquisenseBluetoothKotlinx_coroutines_coreFlow>
@required
@property (readonly) NSArray<id> *replayCache __attribute__((swift_name("replayCache")));
@end

__attribute__((swift_name("Kotlinx_coroutines_coreStateFlow")))
@protocol EquisenseBluetoothKotlinx_coroutines_coreStateFlow <EquisenseBluetoothKotlinx_coroutines_coreSharedFlow>
@required
@property (readonly) id _Nullable value_ __attribute__((swift_name("value_")));
@end

__attribute__((swift_name("Kotlinx_coroutines_coreFlowCollector")))
@protocol EquisenseBluetoothKotlinx_coroutines_coreFlowCollector
@required

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)emitValue:(id _Nullable)value completionHandler:(void (^)(NSError * _Nullable))completionHandler __attribute__((swift_name("emit(value:completionHandler:)")));
@end

__attribute__((swift_name("Kotlinx_coroutines_coreMutableSharedFlow")))
@protocol EquisenseBluetoothKotlinx_coroutines_coreMutableSharedFlow <EquisenseBluetoothKotlinx_coroutines_coreSharedFlow, EquisenseBluetoothKotlinx_coroutines_coreFlowCollector>
@required

/**
 * @note annotations
 *   kotlinx.coroutines.ExperimentalCoroutinesApi
*/
- (void)resetReplayCache __attribute__((swift_name("resetReplayCache()")));
- (BOOL)tryEmitValue:(id _Nullable)value __attribute__((swift_name("tryEmit(value:)")));
@property (readonly) id<EquisenseBluetoothKotlinx_coroutines_coreStateFlow> subscriptionCount __attribute__((swift_name("subscriptionCount")));
@end

__attribute__((swift_name("Kotlinx_coroutines_coreMutableStateFlow")))
@protocol EquisenseBluetoothKotlinx_coroutines_coreMutableStateFlow <EquisenseBluetoothKotlinx_coroutines_coreStateFlow, EquisenseBluetoothKotlinx_coroutines_coreMutableSharedFlow>
@required
- (BOOL)compareAndSetExpect:(id _Nullable)expect update:(id _Nullable)update __attribute__((swift_name("compareAndSet(expect:update:)")));
- (void)setValue:(id _Nullable)value __attribute__((swift_name("setValue(_:)")));
@end

__attribute__((swift_name("KotlinSuspendFunction2")))
@protocol EquisenseBluetoothKotlinSuspendFunction2 <EquisenseBluetoothKotlinFunction>
@required

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)invokeP1:(id _Nullable)p1 p2:(id _Nullable)p2 completionHandler:(void (^)(id _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("invoke(p1:p2:completionHandler:)")));
@end

__attribute__((swift_name("KotlinSuspendFunction1")))
@protocol EquisenseBluetoothKotlinSuspendFunction1 <EquisenseBluetoothKotlinFunction>
@required

/**
 * @note This method converts instances of CancellationException to errors.
 * Other uncaught Kotlin exceptions are fatal.
*/
- (void)invokeP1:(id _Nullable)p1 completionHandler:(void (^)(id _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("invoke(p1:completionHandler:)")));
@end

__attribute__((swift_name("KotlinIterator")))
@protocol EquisenseBluetoothKotlinIterator
@required
- (BOOL)hasNext __attribute__((swift_name("hasNext()")));
- (id _Nullable)next __attribute__((swift_name("next()")));
@end

__attribute__((swift_name("KotlinByteIterator")))
@interface EquisenseBluetoothKotlinByteIterator : EquisenseBluetoothBase <EquisenseBluetoothKotlinIterator>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (EquisenseBluetoothByte *)next __attribute__((swift_name("next()")));
- (int8_t)nextByte __attribute__((swift_name("nextByte()")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_datetimeInstant.Companion")))
@interface EquisenseBluetoothKotlinx_datetimeInstantCompanion : EquisenseBluetoothBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) EquisenseBluetoothKotlinx_datetimeInstantCompanion *shared __attribute__((swift_name("shared")));
- (EquisenseBluetoothKotlinx_datetimeInstant *)fromEpochMillisecondsEpochMilliseconds:(int64_t)epochMilliseconds __attribute__((swift_name("fromEpochMilliseconds(epochMilliseconds:)")));
- (EquisenseBluetoothKotlinx_datetimeInstant *)fromEpochSecondsEpochSeconds:(int64_t)epochSeconds nanosecondAdjustment:(int32_t)nanosecondAdjustment __attribute__((swift_name("fromEpochSeconds(epochSeconds:nanosecondAdjustment:)")));
- (EquisenseBluetoothKotlinx_datetimeInstant *)fromEpochSecondsEpochSeconds:(int64_t)epochSeconds nanosecondAdjustment_:(int64_t)nanosecondAdjustment __attribute__((swift_name("fromEpochSeconds(epochSeconds:nanosecondAdjustment_:)")));
- (EquisenseBluetoothKotlinx_datetimeInstant *)now __attribute__((swift_name("now()"))) __attribute__((unavailable("Use Clock.System.now() instead")));
- (EquisenseBluetoothKotlinx_datetimeInstant *)parseIsoString:(NSString *)isoString __attribute__((swift_name("parse(isoString:)")));
- (id<EquisenseBluetoothKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@property (readonly) EquisenseBluetoothKotlinx_datetimeInstant *DISTANT_FUTURE __attribute__((swift_name("DISTANT_FUTURE")));
@property (readonly) EquisenseBluetoothKotlinx_datetimeInstant *DISTANT_PAST __attribute__((swift_name("DISTANT_PAST")));
@end


/**
 * @note annotations
 *   kotlin.SinceKotlin(version="1.3")
*/
__attribute__((swift_name("KotlinCoroutineContext")))
@protocol EquisenseBluetoothKotlinCoroutineContext
@required
- (id _Nullable)foldInitial:(id _Nullable)initial operation:(id _Nullable (^)(id _Nullable, id<EquisenseBluetoothKotlinCoroutineContextElement>))operation __attribute__((swift_name("fold(initial:operation:)")));
- (id<EquisenseBluetoothKotlinCoroutineContextElement> _Nullable)getKey:(id<EquisenseBluetoothKotlinCoroutineContextKey>)key __attribute__((swift_name("get(key:)")));
- (id<EquisenseBluetoothKotlinCoroutineContext>)minusKeyKey:(id<EquisenseBluetoothKotlinCoroutineContextKey>)key __attribute__((swift_name("minusKey(key:)")));
- (id<EquisenseBluetoothKotlinCoroutineContext>)plusContext:(id<EquisenseBluetoothKotlinCoroutineContext>)context __attribute__((swift_name("plus(context:)")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreSerializationStrategy")))
@protocol EquisenseBluetoothKotlinx_serialization_coreSerializationStrategy
@required
- (void)serializeEncoder:(id<EquisenseBluetoothKotlinx_serialization_coreEncoder>)encoder value:(id _Nullable)value __attribute__((swift_name("serialize(encoder:value:)")));
@property (readonly) id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreDeserializationStrategy")))
@protocol EquisenseBluetoothKotlinx_serialization_coreDeserializationStrategy
@required
- (id _Nullable)deserializeDecoder:(id<EquisenseBluetoothKotlinx_serialization_coreDecoder>)decoder __attribute__((swift_name("deserialize(decoder:)")));
@property (readonly) id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreKSerializer")))
@protocol EquisenseBluetoothKotlinx_serialization_coreKSerializer <EquisenseBluetoothKotlinx_serialization_coreSerializationStrategy, EquisenseBluetoothKotlinx_serialization_coreDeserializationStrategy>
@required
@end

__attribute__((swift_name("KotlinCoroutineContextElement")))
@protocol EquisenseBluetoothKotlinCoroutineContextElement <EquisenseBluetoothKotlinCoroutineContext>
@required
@property (readonly) id<EquisenseBluetoothKotlinCoroutineContextKey> key __attribute__((swift_name("key")));
@end

__attribute__((swift_name("KotlinCoroutineContextKey")))
@protocol EquisenseBluetoothKotlinCoroutineContextKey
@required
@end

__attribute__((swift_name("Kotlinx_serialization_coreEncoder")))
@protocol EquisenseBluetoothKotlinx_serialization_coreEncoder
@required
- (id<EquisenseBluetoothKotlinx_serialization_coreCompositeEncoder>)beginCollectionDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)descriptor collectionSize:(int32_t)collectionSize __attribute__((swift_name("beginCollection(descriptor:collectionSize:)")));
- (id<EquisenseBluetoothKotlinx_serialization_coreCompositeEncoder>)beginStructureDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("beginStructure(descriptor:)")));
- (void)encodeBooleanValue:(BOOL)value __attribute__((swift_name("encodeBoolean(value:)")));
- (void)encodeByteValue:(int8_t)value __attribute__((swift_name("encodeByte(value:)")));
- (void)encodeCharValue:(unichar)value __attribute__((swift_name("encodeChar(value:)")));
- (void)encodeDoubleValue:(double)value __attribute__((swift_name("encodeDouble(value:)")));
- (void)encodeEnumEnumDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)enumDescriptor index:(int32_t)index __attribute__((swift_name("encodeEnum(enumDescriptor:index:)")));
- (void)encodeFloatValue:(float)value __attribute__((swift_name("encodeFloat(value:)")));
- (id<EquisenseBluetoothKotlinx_serialization_coreEncoder>)encodeInlineInlineDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)inlineDescriptor __attribute__((swift_name("encodeInline(inlineDescriptor:)")));
- (void)encodeIntValue:(int32_t)value __attribute__((swift_name("encodeInt(value:)")));
- (void)encodeLongValue:(int64_t)value __attribute__((swift_name("encodeLong(value:)")));
- (void)encodeNotNullMark __attribute__((swift_name("encodeNotNullMark()")));
- (void)encodeNull __attribute__((swift_name("encodeNull()")));
- (void)encodeNullableSerializableValueSerializer:(id<EquisenseBluetoothKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeNullableSerializableValue(serializer:value:)")));
- (void)encodeSerializableValueSerializer:(id<EquisenseBluetoothKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeSerializableValue(serializer:value:)")));
- (void)encodeShortValue:(int16_t)value __attribute__((swift_name("encodeShort(value:)")));
- (void)encodeStringValue:(NSString *)value __attribute__((swift_name("encodeString(value:)")));
@property (readonly) EquisenseBluetoothKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreSerialDescriptor")))
@protocol EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor
@required
- (NSArray<id<EquisenseBluetoothKotlinAnnotation>> *)getElementAnnotationsIndex:(int32_t)index __attribute__((swift_name("getElementAnnotations(index:)")));
- (id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)getElementDescriptorIndex:(int32_t)index __attribute__((swift_name("getElementDescriptor(index:)")));
- (int32_t)getElementIndexName:(NSString *)name __attribute__((swift_name("getElementIndex(name:)")));
- (NSString *)getElementNameIndex:(int32_t)index __attribute__((swift_name("getElementName(index:)")));
- (BOOL)isElementOptionalIndex:(int32_t)index __attribute__((swift_name("isElementOptional(index:)")));
@property (readonly) NSArray<id<EquisenseBluetoothKotlinAnnotation>> *annotations __attribute__((swift_name("annotations")));
@property (readonly) int32_t elementsCount __attribute__((swift_name("elementsCount")));
@property (readonly) BOOL isInline __attribute__((swift_name("isInline")));
@property (readonly) BOOL isNullable __attribute__((swift_name("isNullable")));
@property (readonly) EquisenseBluetoothKotlinx_serialization_coreSerialKind *kind __attribute__((swift_name("kind")));
@property (readonly) NSString *serialName __attribute__((swift_name("serialName")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreDecoder")))
@protocol EquisenseBluetoothKotlinx_serialization_coreDecoder
@required
- (id<EquisenseBluetoothKotlinx_serialization_coreCompositeDecoder>)beginStructureDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("beginStructure(descriptor:)")));
- (BOOL)decodeBoolean __attribute__((swift_name("decodeBoolean()")));
- (int8_t)decodeByte __attribute__((swift_name("decodeByte()")));
- (unichar)decodeChar __attribute__((swift_name("decodeChar()")));
- (double)decodeDouble __attribute__((swift_name("decodeDouble()")));
- (int32_t)decodeEnumEnumDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)enumDescriptor __attribute__((swift_name("decodeEnum(enumDescriptor:)")));
- (float)decodeFloat __attribute__((swift_name("decodeFloat()")));
- (id<EquisenseBluetoothKotlinx_serialization_coreDecoder>)decodeInlineInlineDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)inlineDescriptor __attribute__((swift_name("decodeInline(inlineDescriptor:)")));
- (int32_t)decodeInt __attribute__((swift_name("decodeInt()")));
- (int64_t)decodeLong __attribute__((swift_name("decodeLong()")));
- (BOOL)decodeNotNullMark __attribute__((swift_name("decodeNotNullMark()")));
- (EquisenseBluetoothKotlinNothing * _Nullable)decodeNull __attribute__((swift_name("decodeNull()")));
- (id _Nullable)decodeNullableSerializableValueDeserializer:(id<EquisenseBluetoothKotlinx_serialization_coreDeserializationStrategy>)deserializer __attribute__((swift_name("decodeNullableSerializableValue(deserializer:)")));
- (id _Nullable)decodeSerializableValueDeserializer:(id<EquisenseBluetoothKotlinx_serialization_coreDeserializationStrategy>)deserializer __attribute__((swift_name("decodeSerializableValue(deserializer:)")));
- (int16_t)decodeShort __attribute__((swift_name("decodeShort()")));
- (NSString *)decodeString __attribute__((swift_name("decodeString()")));
@property (readonly) EquisenseBluetoothKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreCompositeEncoder")))
@protocol EquisenseBluetoothKotlinx_serialization_coreCompositeEncoder
@required
- (void)encodeBooleanElementDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(BOOL)value __attribute__((swift_name("encodeBooleanElement(descriptor:index:value:)")));
- (void)encodeByteElementDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int8_t)value __attribute__((swift_name("encodeByteElement(descriptor:index:value:)")));
- (void)encodeCharElementDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(unichar)value __attribute__((swift_name("encodeCharElement(descriptor:index:value:)")));
- (void)encodeDoubleElementDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(double)value __attribute__((swift_name("encodeDoubleElement(descriptor:index:value:)")));
- (void)encodeFloatElementDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(float)value __attribute__((swift_name("encodeFloatElement(descriptor:index:value:)")));
- (id<EquisenseBluetoothKotlinx_serialization_coreEncoder>)encodeInlineElementDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("encodeInlineElement(descriptor:index:)")));
- (void)encodeIntElementDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int32_t)value __attribute__((swift_name("encodeIntElement(descriptor:index:value:)")));
- (void)encodeLongElementDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int64_t)value __attribute__((swift_name("encodeLongElement(descriptor:index:value:)")));
- (void)encodeNullableSerializableElementDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index serializer:(id<EquisenseBluetoothKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeNullableSerializableElement(descriptor:index:serializer:value:)")));
- (void)encodeSerializableElementDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index serializer:(id<EquisenseBluetoothKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeSerializableElement(descriptor:index:serializer:value:)")));
- (void)encodeShortElementDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int16_t)value __attribute__((swift_name("encodeShortElement(descriptor:index:value:)")));
- (void)encodeStringElementDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(NSString *)value __attribute__((swift_name("encodeStringElement(descriptor:index:value:)")));
- (void)endStructureDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("endStructure(descriptor:)")));
- (BOOL)shouldEncodeElementDefaultDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("shouldEncodeElementDefault(descriptor:index:)")));
@property (readonly) EquisenseBluetoothKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreSerializersModule")))
@interface EquisenseBluetoothKotlinx_serialization_coreSerializersModule : EquisenseBluetoothBase
- (void)dumpToCollector:(id<EquisenseBluetoothKotlinx_serialization_coreSerializersModuleCollector>)collector __attribute__((swift_name("dumpTo(collector:)")));
- (id<EquisenseBluetoothKotlinx_serialization_coreKSerializer> _Nullable)getContextualKClass:(id<EquisenseBluetoothKotlinKClass>)kClass typeArgumentsSerializers:(NSArray<id<EquisenseBluetoothKotlinx_serialization_coreKSerializer>> *)typeArgumentsSerializers __attribute__((swift_name("getContextual(kClass:typeArgumentsSerializers:)")));
- (id<EquisenseBluetoothKotlinx_serialization_coreSerializationStrategy> _Nullable)getPolymorphicBaseClass:(id<EquisenseBluetoothKotlinKClass>)baseClass value:(id)value __attribute__((swift_name("getPolymorphic(baseClass:value:)")));
- (id<EquisenseBluetoothKotlinx_serialization_coreDeserializationStrategy> _Nullable)getPolymorphicBaseClass:(id<EquisenseBluetoothKotlinKClass>)baseClass serializedClassName:(NSString * _Nullable)serializedClassName __attribute__((swift_name("getPolymorphic(baseClass:serializedClassName:)")));
@end

__attribute__((swift_name("KotlinAnnotation")))
@protocol EquisenseBluetoothKotlinAnnotation
@required
@end

__attribute__((swift_name("Kotlinx_serialization_coreSerialKind")))
@interface EquisenseBluetoothKotlinx_serialization_coreSerialKind : EquisenseBluetoothBase
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@end

__attribute__((swift_name("Kotlinx_serialization_coreCompositeDecoder")))
@protocol EquisenseBluetoothKotlinx_serialization_coreCompositeDecoder
@required
- (BOOL)decodeBooleanElementDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeBooleanElement(descriptor:index:)")));
- (int8_t)decodeByteElementDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeByteElement(descriptor:index:)")));
- (unichar)decodeCharElementDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeCharElement(descriptor:index:)")));
- (int32_t)decodeCollectionSizeDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("decodeCollectionSize(descriptor:)")));
- (double)decodeDoubleElementDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeDoubleElement(descriptor:index:)")));
- (int32_t)decodeElementIndexDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("decodeElementIndex(descriptor:)")));
- (float)decodeFloatElementDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeFloatElement(descriptor:index:)")));
- (id<EquisenseBluetoothKotlinx_serialization_coreDecoder>)decodeInlineElementDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeInlineElement(descriptor:index:)")));
- (int32_t)decodeIntElementDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeIntElement(descriptor:index:)")));
- (int64_t)decodeLongElementDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeLongElement(descriptor:index:)")));
- (id _Nullable)decodeNullableSerializableElementDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index deserializer:(id<EquisenseBluetoothKotlinx_serialization_coreDeserializationStrategy>)deserializer previousValue:(id _Nullable)previousValue __attribute__((swift_name("decodeNullableSerializableElement(descriptor:index:deserializer:previousValue:)")));
- (BOOL)decodeSequentially __attribute__((swift_name("decodeSequentially()")));
- (id _Nullable)decodeSerializableElementDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index deserializer:(id<EquisenseBluetoothKotlinx_serialization_coreDeserializationStrategy>)deserializer previousValue:(id _Nullable)previousValue __attribute__((swift_name("decodeSerializableElement(descriptor:index:deserializer:previousValue:)")));
- (int16_t)decodeShortElementDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeShortElement(descriptor:index:)")));
- (NSString *)decodeStringElementDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeStringElement(descriptor:index:)")));
- (void)endStructureDescriptor:(id<EquisenseBluetoothKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("endStructure(descriptor:)")));
@property (readonly) EquisenseBluetoothKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinNothing")))
@interface EquisenseBluetoothKotlinNothing : EquisenseBluetoothBase
@end

__attribute__((swift_name("Kotlinx_serialization_coreSerializersModuleCollector")))
@protocol EquisenseBluetoothKotlinx_serialization_coreSerializersModuleCollector
@required
- (void)contextualKClass:(id<EquisenseBluetoothKotlinKClass>)kClass provider:(id<EquisenseBluetoothKotlinx_serialization_coreKSerializer> (^)(NSArray<id<EquisenseBluetoothKotlinx_serialization_coreKSerializer>> *))provider __attribute__((swift_name("contextual(kClass:provider:)")));
- (void)contextualKClass:(id<EquisenseBluetoothKotlinKClass>)kClass serializer:(id<EquisenseBluetoothKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("contextual(kClass:serializer:)")));
- (void)polymorphicBaseClass:(id<EquisenseBluetoothKotlinKClass>)baseClass actualClass:(id<EquisenseBluetoothKotlinKClass>)actualClass actualSerializer:(id<EquisenseBluetoothKotlinx_serialization_coreKSerializer>)actualSerializer __attribute__((swift_name("polymorphic(baseClass:actualClass:actualSerializer:)")));
- (void)polymorphicDefaultBaseClass:(id<EquisenseBluetoothKotlinKClass>)baseClass defaultDeserializerProvider:(id<EquisenseBluetoothKotlinx_serialization_coreDeserializationStrategy> _Nullable (^)(NSString * _Nullable))defaultDeserializerProvider __attribute__((swift_name("polymorphicDefault(baseClass:defaultDeserializerProvider:)")));
- (void)polymorphicDefaultDeserializerBaseClass:(id<EquisenseBluetoothKotlinKClass>)baseClass defaultDeserializerProvider:(id<EquisenseBluetoothKotlinx_serialization_coreDeserializationStrategy> _Nullable (^)(NSString * _Nullable))defaultDeserializerProvider __attribute__((swift_name("polymorphicDefaultDeserializer(baseClass:defaultDeserializerProvider:)")));
- (void)polymorphicDefaultSerializerBaseClass:(id<EquisenseBluetoothKotlinKClass>)baseClass defaultSerializerProvider:(id<EquisenseBluetoothKotlinx_serialization_coreSerializationStrategy> _Nullable (^)(id))defaultSerializerProvider __attribute__((swift_name("polymorphicDefaultSerializer(baseClass:defaultSerializerProvider:)")));
@end

__attribute__((swift_name("KotlinKDeclarationContainer")))
@protocol EquisenseBluetoothKotlinKDeclarationContainer
@required
@end

__attribute__((swift_name("KotlinKAnnotatedElement")))
@protocol EquisenseBluetoothKotlinKAnnotatedElement
@required
@end


/**
 * @note annotations
 *   kotlin.SinceKotlin(version="1.1")
*/
__attribute__((swift_name("KotlinKClassifier")))
@protocol EquisenseBluetoothKotlinKClassifier
@required
@end

__attribute__((swift_name("KotlinKClass")))
@protocol EquisenseBluetoothKotlinKClass <EquisenseBluetoothKotlinKDeclarationContainer, EquisenseBluetoothKotlinKAnnotatedElement, EquisenseBluetoothKotlinKClassifier>
@required

/**
 * @note annotations
 *   kotlin.SinceKotlin(version="1.1")
*/
- (BOOL)isInstanceValue:(id _Nullable)value __attribute__((swift_name("isInstance(value:)")));
@property (readonly) NSString * _Nullable qualifiedName __attribute__((swift_name("qualifiedName")));
@property (readonly) NSString * _Nullable simpleName __attribute__((swift_name("simpleName")));
@end

#pragma pop_macro("_Nullable_result")
#pragma clang diagnostic pop
NS_ASSUME_NONNULL_END
